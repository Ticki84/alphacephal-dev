class Othellier():
    """ Définition de la classe Othellier

    Ensemble des méthodes permettant de définir la logique sur l'othellier
    Données:
        1: noir, -1: blanc, 0: vide
        Coups et cases sont stockés sous forme de tuples (y, x) où y est la ligne, x la colonne
    """
    directions = [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)]

    def __init__(self, n):
        self.n = n

        # On crée l'othellier vide
        self.othellier = [None] * self.n
        for y in range(self.n):
            self.othellier[y] = [0] * self.n

        # On place les 4 pions initiaux
        self.othellier[self.n // 2 - 1][self.n // 2] = 1
        self.othellier[self.n // 2][self.n // 2 - 1] = 1
        self.othellier[self.n // 2 - 1][self.n // 2 - 1] = -1
        self.othellier[self.n // 2][self.n // 2] = -1
        # Partie avortée
        # self.othellier[self.n - 1][self.n - 1] = -1
        # self.othellier[self.n - 1][self.n - 2] = 1
        # Match nul
        # self.othellier[0][0] = 1
        # self.othellier[self.n - 1][self.n - 1] = -1

    def __getitem__(self, index):
        """ Ajout de l'indexeur [][] (de la variable membre othellier) à la classe Othellier """
        return self.othellier[index]

    def compterScores(self):
        """ Compte le score des deux joueurs

        Returns:
            (int, int) -- Tuple contenant les scores (joueur 1, joueur 2)
        """
        score1 = 0
        score2 = 0
        for y in range(self.n):
            for x in range(self.n):
                if self[y][x] == 1:
                    score1 += 1
                if self[y][x] == -1:
                    score2 += 1
        return (score1, score2)

    def getCoupsJoueur(self, joueur):
        """ Permet d'obtenir les coups jouables par un joueur

        Arguments:
            joueur {int} -- 1: noir, -1: blanc

        Returns:
            [(y, x)] -- Liste contenant tous les coups jouables par le joueur
        """
        coups = set()  # On crée un set pour éviter les doublons

        for y in range(self.n):
            for x in range(self.n):
                if self[y][x] == joueur:
                    nCoups = self.getCoupsCase((y, x))
                    coups.update(nCoups)
        return list(coups)

    def aCoupValide(self, joueur):
        """ Retourne si le joueur peut jouer

        Arguments:
            joueur {int} -- 1: noir, -1: blanc

        Returns:
            bool -- True si le joueur peut jouer (au moins) un coup
        """
        for y in range(self.n):
            for x in range(self.n):
                if self[y][x] == joueur:
                    if len(self.getCoupsCase((y, x))) > 0:
                        return True
        return False

    def getCoupsCase(self, case):
        """ Retourne tous les coups jouables à partir d'une case

        Arguments:
            case {(y, x)} -- Case de coordonnées (y, x)

        Returns:
            [(y, x)] -- Liste contenant les coups jouables depuis la case
        """
        (y, x) = case
        joueur = self[y][x]

        if joueur == 0:
            return None

        coups = []
        for direction in self.directions:
            coup = self.explorerCoup(case, direction)
            if coup:
                coups.append(coup)
        return coups

    def jouerCoup(self, coup, joueur):
        """ Joue le coup pour le joueur choisi, en retournant les pions sur l'othellier

        Arguments:
            coup {(y, x)} -- Coup (y, x) à jouer
            joueur {int} -- 1: noir, -1: blanc

        Keyword Arguments:
            interface {Interface} -- Interface graphique (default: {None})
        """
        retournements = [retournement for direction in self.directions
                         for retournement in self.getRetournements(coup, direction, joueur)]
        assert len(list(retournements)) > 0
        for y, x in retournements:
            self[y][x] = joueur

    def explorerCoup(self, origine, direction):
        """ Retourne la case de fin d'un coup, en partant dans la direction choisie
        depuis la case d'origine du coup

        Arguments:
            origine {(y, x)} -- Case d'origine du coup
            direction {(int, int)} -- Direction à explorer

        Returns:
            (y, x) -- Case où se termine le coup, None si le coup n'est pas valide (origine ou direction)
        """
        y, x = origine
        joueur = self[y][x]
        retournements = []

        for y, x in Othellier.incrementerCoup(origine, direction, self.n):
            if self[y][x] == 0:
                if retournements:
                    return (y, x)
                else:
                    return None
            elif self[y][x] == joueur:
                return None
            elif self[y][x] == -joueur:
                retournements.append((y, x))

    def getRetournements(self, origine, direction, joueur):
        """ Retourne la liste des pions retournés en jouant un coup

        Arguments:
            origine {(y, x)} -- Case d'origine du coup
            direction {(int, int)} -- Direction du coup
            joueur {int} -- 1: noir, -1: blanc

        Returns:
            [(y, x)] -- Liste des pions à retourner
        """
        retournements = [origine]

        for y, x in Othellier.incrementerCoup(origine, direction, self.n):
            if self[y][x] == 0:
                return []
            if self[y][x] == -joueur:
                retournements.append((y, x))
            elif self[y][x] == joueur and len(retournements) > 0:
                return retournements

        return []

    @staticmethod
    def incrementerCoup(coup, direction, n):
        """ Générateur pour incrémenter un coup dans la direction voulue et dans la limite de l'othellier

        Arguments:
            coup {(y, x)} -- Origine du coup
            direction {(int, int)} -- Direction du coup
            n {int} -- Dimension de l'othellier

        Yields:
            [(y, x)] -- Liste des pions (dans la limite de l'othellier)
        """
        coup = list(map(sum, zip(coup, direction)))  # On somme chacun des éléments avec la direction une première fois
        # coup = (coup[0] + direction[0], coup[1] + direction[1])
        while all(map(lambda x: 0 <= x < n, coup)):  # Tant que les coordonnées sont valides
            # while 0 <= coup[0] and coup[0] < n and 0 <= coup[1] and coup[1] < n:
            yield coup  # On rajoute le coup, la boucle continue
            coup = list(map(sum, zip(coup, direction)))  # On somme chacun des éléments avec la direction autant de fois que possible
            # coup = (coup[0] + direction[0], coup[1] + direction[1])
