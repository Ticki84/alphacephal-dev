import numpy as np

"""
Ce fichier contient les méthodes des styles de jeux (aléatoire, humain et gourmand)
"""


class JoueurAleatoire():
    def __init__(self, othello):
        self.othello = othello

    def jouer(self, othellier):
        """ Joue aléatoirement une case

        Arguments:
            othellier {Othellier} -- Othellier actuel

        Returns:
            int -- Case où jouer
        """
        case = np.random.randint(self.othello.getNombreActions())
        coups = self.othello.getCoupsValides(othellier, 1)
        while coups[case] != 1:
            case = np.random.randint(self.othello.getNombreActions())
        return case


class JoueurHumain():
    def __init__(self, othello):
        self.othello = othello

    def jouer(self, othellier):
        """ Demande la saisie de la case au joueur, l'entrée est vérifiée

        Arguments:
            othellier {Othellier} -- Othellier actuel

        Returns:
            int -- Case où jouer
        """
        coups = self.othello.getCoupsValides(othellier, 1)
        if coups[-1] == 1:
            return len(coups) - 1  # Si le joueur ne peut pas jouer, on lui fait passer son tour
        while True:
            case = input("Dans quelle case voulez-vous jouer? ")

            if case.isdigit() and len(case) == 2:
                x, y = int(case[1]), int(case[0])
                case = self.othello.n * y + x if y != -1 else self.othello.n ** 2
                if coups[case]:
                    break
            print("Case invalide!")

        return case


class JoueurGourmand():
    def __init__(self, othello):
        self.othello = othello

    def jouer(self, othellier):
        """ Joue les cases offrant le plus de score

        Arguments:
            othellier {Othellier} -- Othellier actuel

        Returns:
            int -- Case où jouer
        """
        coups = self.othello.getCoupsValides(othellier, 1)
        coupsCandidats = []
        for coup in range(self.othello.getNombreActions()):
            if coups[coup] == 0:
                continue
            prochainOthellier, _ = self.othello.getProchainEtat(othellier, 1, coup)
            scores = self.othello.getScores(prochainOthellier)
            score = scores[1] - scores[0]
            coupsCandidats += [(-score, coup)]
        coupsCandidats.sort()
        return coupsCandidats[0][1]
