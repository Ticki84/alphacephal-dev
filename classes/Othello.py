from __future__ import print_function
import numpy as np
from classes.Othellier import Othellier
import sys
sys.path.append('..')


class Othello():
    """ Cette classe définie la base d'un Othello """

    def __init__(self, n):
        # Les othelliers de plus de 10x10 ne sont pas gérés (à cause de l'input de case sous forme yx par l'utilisateur), de même les dimensions doivent être paires
        assert n < 11 and n % 2 == 0
        self.n = n

    def getOthellierInitial(self):
        """ Retourne l'othellier initial

        Returns:
            np.array -- Othellier sous forme de matrice (sa forme
                        d'entrée pour le réseau neuronal)
        """
        d = Othellier(self.n)
        return np.array(d.othellier)

    def getDimensionsOthellier(self):
        """ Retourne les dimensions de l'othellier

        Returns:
            (int, int) -- Dimensions (x, y) d'othellier
        """
        return (self.n, self.n)

    def getNombreActions(self):
        """ Retourne le nombre total d'actions possibles

        Returns:
            int -- Nombre d'actions
        """
        return self.n**2 + 1

    def getProchainEtat(self, othellier, joueur, action):
        """ Renvoie le prochain état du jeu

        Arguments:
            othellier {np.array} -- Othellier actuel
            joueur {int} -- 1: noir, -1: blanc
            action {int} -- n° de la case (où le joueur va jouer)

        Keyword Arguments:
            interface {Interface} -- Interface graphique (default: {None})

        Returns:
            (np.array, int) -- Tuple (othellier, joueur) contenant l'othellier et
                               le joueur pour le tour suivant
        """
        if action == self.n**2:  # Si le joueur ne peut pas jouer, on lui fait passer son tour
            return (othellier, -joueur)
        d = Othellier(self.n)
        d.othellier = np.copy(othellier)
        coup = (action // self.n, action % self.n)
        d.jouerCoup(coup, joueur)
        return (d.othellier, -joueur)

    def getCoupsValides(self, othellier, joueur):
        """ Renvoie un vecteur informant des coups valides ou non pour le joueur

        Arguments:
            othellier {np.array} -- Othellier actuel
            joueur {int} -- 1: noir, -1: blanc

        Returns:
            np.array -- Vecteur de dimension self.getNombreActions() contenant, pour l'othellier
                        et le joueur actuel, les valeurs:
                        1: coup valide (jouable), 0: coup invalide
        """
        coupsValides = [0] * self.getNombreActions()
        d = Othellier(self.n)
        d.othellier = np.copy(othellier)
        coups = d.getCoupsJoueur(joueur)
        if len(coups) == 0:
            coupsValides[-1] = 1  # Si le joueur ne peut pas jouer, alors on met le coup (self.othello.n, self.othello.n) vaut 1
            return np.array(coupsValides)
        for y, x in coups:
            coupsValides[self.n * y + x] = 1
        return np.array(coupsValides)

    def getFinPartie(self, othellier, joueur):
        """ Retourne l'état actuel de la partie

        Arguments:
            othellier {np.array} -- Othellier actuel
            joueur {int} -- 1: noir, -1: blanc

        Returns:
            float -- 0: en cours, 1: le joueur a gagné, -1: le joueur a perdu, -0.5: match nul
        """
        d = Othellier(self.n)
        d.othellier = np.copy(othellier)
        if d.aCoupValide(joueur) or d.aCoupValide(-joueur):
            return 0
        scores = d.compterScores()
        if scores[0] > scores[1]:
            return 1
        elif scores[0] == scores[1]:
            return -0.5
        return -1

    def getFormeCanonique(self, othellier, joueur):
        """ Retourne la forme canonique

        La forme canonique est indépendante du joueur. Par exemple,
        si on prend l'othellier du point de vue du joueur noir et que
        le joueur est le noir, on renvoie l'othellier tel quel. Sinon
        si le joueur est blanc, on inverse la couleur des pions et
        on renvoie l'othellier

        Arguments:
            othellier {np.array} -- Othellier actuel
            joueur {int} -- 1: noir, -1: blanc

        Returns:
            np.array -- Othellier sous forme canonique
        """
        return joueur * othellier

    def getSymetries(self, othellier, Pi):
        """ Renvoie une liste de formes symétriques

        Utilisé lors de l'entraînement du réseau neuronal.
        Formes de symétries: rotation, miroir

        Arguments:
            othellier {np.array} -- Othellier actuel
            Pi {np.array} -- Vecteur ligne des probabilités, de dimension self.getNombreActions()

        Returns:
            [(np.array, np.array)] -- Liste où chaque tuple est une forme symétrique de l'othellier
                                      et son vecteur des probabilités Pi associé
        """
        assert(len(Pi) == self.n**2 + 1)  # On vérifie que le vecteur Pi est de dimension égale à l'othellier actuel
        PiOthellier = np.reshape(Pi[:-1], (self.n, self.n))  # On remet Pi sous forme matricielle
        symetries = []

        for rot in range(1, 5):  # Pour les 4 rotations de 90° (jusqu'à 360°)
            for alt in [True, False]:
                # Rotations de 90°
                nouvOthellier = np.rot90(othellier, rot)  # On réécrie les variables pour éviter de créer des vues (on crée des copies pour éviter la propagation)
                nouvPi = np.rot90(PiOthellier, rot)
                if alt:  # Symétries verticales (gauche/droite)
                    nouvOthellier = np.fliplr(nouvOthellier)
                    nouvPi = np.fliplr(nouvPi)
                symetries += [(nouvOthellier, list(nouvPi.ravel()) + [Pi[-1]])]  # On ajoute les symétries, on remet Pi en vecteur ligne
        return symetries

    def getRepresentation(self, othellier):
        """ Retourne la représentation canonique du othellier en bytes

        Requis pour le hashage lors du MCTS

        Arguments:
            othellier {np.array} -- Othellier actuel

        Returns:
            string -- Othellier sous forme canonique converti en bytes
        """
        return othellier.tostring()

    def getScores(self, othellier):
        """ Retourne le score des joueurs

        Arguments:
            othellier {np.array} -- Othellier actuel

        Returns:
            (int, int) -- Tuple contenant les deux scores (J1, J2)
        """
        d = Othellier(self.n)
        d.othellier = np.copy(othellier)
        return d.compterScores()

    def afficher(self, othellier, joueur):
        """ Affiche l'othellier dans la console

        Arguments:
            othellier {np.array} -- Othellier actuel
            joueur {int} -- 1: noir, -1: blanc
        """
        n = othellier.shape[0]  # renvoie la dimension en y de l'othellier
        coups = self.getCoupsValides(othellier, joueur)

        for y in range(n):
            print(y, "|", end="")
            for x in range(n):
                if othellier[y][x] == 1:
                    print("X", end="")
                elif othellier[y][x] == -1:
                    print("O", end="")
                elif coups[y * n + x]:
                    print(".", end="")
                else:
                    print(" ", end="")
                print("|", end="")
            print("")
        dernLigne = "  "
        for x in range(n):
            dernLigne += " " + str(x)
        print(dernLigne)
