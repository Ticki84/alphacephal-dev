from collections import deque, Counter
from classes.Arene import Arene
from classes.MCTS import MCTS
import numpy as np
import os
from sys import exit
from pickle import Pickler, Unpickler
from random import shuffle
import multiprocessing
from classes.ReseauNeuro import ReseauNeuro
from tqdm import tqdm_notebook as tqdm


def effectuerPartie(othello, args, ignorerErr):
    """ Version asynchrone

    Cette fonction effectue la partie du réseau contre lui-même, en commançant par le joueur 1,
    pour renvoyer une liste d'exemples

    Quand le tour a été joué, il est ajouté en tant qu'exemple d'entraînement.
    Une fois la partie finie, le résultat de la partie est assigné à chaque exemple dans
    le fichier d'exemples.
    La fonction utilise diviseur=1 si tour < seuilTour, puis par la suite diviseur=0, où diviseur
    est le dénominateur de la puissance des probabilités

    Arguments:
        othello {Othello} -- Othello
        args {dotdict} -- Arguments de 'entrainer.py'
        ignorerErr {bool} -- True si on peut ignorer les erreurs de chargement

    Returns:
        [(np.array, np.array, int)] -- Liste d'exemples de la forme (othellierCanonique, Pi, v)
                                       Pi est le vecteur des estimations renvoyé par la MCTS, v
                                       vaut +1 si le joueur a gagné, -1 sinon
    """
    resNeuro = ReseauNeuro(othello)
    mcts = MCTS(othello, resNeuro, args)
    try:
        resNeuro.chargerReprise(args.reprise, 'recent.pth.tar')
    except:
        if not ignorerErr:
            exit("Partie: échec du chargement du dernier modèle 'recent.pth.tar'")
        pass

    exemples = []
    othellier = othello.getOthellierInitial()
    joueurActuel = 1
    tour = 0

    while True:
        tour += 1
        othellierCanonique = othello.getFormeCanonique(othellier, joueurActuel)
        diviseur = int(tour < args.seuilTour)

        # On ajoute toutes les symétries possibles à la liste d'exemples
        Pi = mcts.getProbaAction(othellierCanonique, diviseur)
        symetries = othello.getSymetries(othellierCanonique, Pi)
        for o, n in symetries:
            exemples.append([o, joueurActuel, n, None])

        # On effectue une des actions au hasard
        action = np.random.choice(len(Pi), p=Pi)
        othellier, joueurActuel = othello.getProchainEtat(othellier, joueurActuel, action)

        # On regarde si la partie est finie
        v = othello.getFinPartie(othellier, joueurActuel)
        if v != 0:
            # Si oui on retourne les exemples
            v = v if v != -0.5 else -1  # On considère le match nul comme une défaite pour éviter d'en faire
            return [(exemple[0], exemple[2], v * ((-1)**(exemple[1] != joueurActuel))) for exemple in exemples]


def comparaison(othello, args, inverse):
    """ Effectue les comparaisons entre le modèle d'entraînement et le dernier modèle approuvé

    Arguments:
        othello {Othello} -- Othello
        args {dotdict} -- Arguments de 'entrainer.py'
        inverse {bool} -- True si on est dans la 2ème moitié des parties à jouer

    Returns:
        int -- 1: victoire du précédent modèle, -1: victoire du nouveau modèle, : match nul
    """
    resNeuro = ReseauNeuro(othello)
    advNeuro = ReseauNeuro(othello)
    try:
        resNeuro.chargerReprise(args.reprise, 'entrainement.pth.tar')  # Le réseau neuronal prend le modèle nouvellement créé
    except:
        exit("Comparaison: échec du chargement du modèle d'entraînement 'entrainement.pth.tar'")
    try:
        advNeuro.chargerReprise(args.reprise, 'ancien.pth.tar')  # Le réseau adverse prend l'ancien meilleur modèle
    except:
        exit("Comparaison: échec du chargement du précédent modèle 'ancien.pth.tar'")
    advmtcs = MCTS(othello, advNeuro, args)
    resmtcs = MCTS(othello, resNeuro, args)

    arene = Arene(lambda proba: np.argmax(advmtcs.getProbaAction(proba, 0)),
                  lambda proba: np.argmax(resmtcs.getProbaAction(proba, 0)), othello) if not inverse else \
        Arene(lambda proba: np.argmax(resmtcs.getProbaAction(proba, 0)),
              lambda proba: np.argmax(advmtcs.getProbaAction(proba, 0)), othello)
    return (-1 if inverse else 1) * arene.jouerPartie()


class Entraineur():
    """
    Cette classe effectue le machine-learning en faisant jouer un réseau neuronal contre lui-même.
    Elle utilise les fonctions définies dans Othello et ReseauNeuronal.
    Les arguments utilisés sont spécifiés dans entrainer.py
    """

    def __init__(self, othello, args):
        self.othello = othello
        self.args = args
        self.historiqueExemples = []                                        # Historique des exemples des seuilHistoModeles derniers modèles
        self.passerPremierePartie = False                                   # Peut être modifiée dans chargerExemples()

    def sauverExemples(self):
        """ Sauvegarde le fichier d'exemples (.pth.tar.exp)
        """
        dossier = self.args.reprise
        if not os.path.exists(dossier):
            os.makedirs(dossier)
        nomFichier = os.path.join(dossier, "historiqueExemples.pth.tar.exp")
        with open(nomFichier, "wb+") as fichier:
            Pickler(fichier).dump(self.historiqueExemples)
        fichier.closed

    def chargerExemples(self):
        """ Charge le fichier d'exemples (.ptr.tar.exp)
        """
        fichierExemples = os.path.join(self.args.reprise, "historiqueExemples.pth.tar.exp")
        if os.path.isfile(fichierExemples):
            print("Fichier d'exemples trouvé, lecture.")
            with open(fichierExemples, "rb") as fichier:
                self.historiqueExemples = Unpickler(fichier).load()
            fichier.closed
            # Les exemples basés sur le modèle ont été collecté et chargé
            self.passerPremierePartie = True
        else:
            exit("chargerModele = True, cependant le fichier 'historiqueExemples.pth.tar.exp' n'a pas été trouvé!")

    def getFichierReprise(self, iteration):
        """ Renvoie le nom du fichier de reprise (.pth.tar)

        Arguments:
            iteration {int} -- N° d'itération
        """
        return 'reprise_' + str(iteration) + '.pth.tar'

    def asyncEffectuerPartie(self, ignorerErr):
        """ Permet de lancer des parties pour accumuler des exemples de manière asynchrone

        Arguments:
            ignorerErr {bool} -- True si on peut ignorer les erreurs de chargement
        """
        pool = multiprocessing.Pool(processes=self.args.nbCPU)
        res = []
        resultat = []
        temp = []
        barre = tqdm(total=self.args.nbParties, desc="Ajout d'exemples", unit="partie", leave=False)
        barre.set_postfix(cpu=self.args.nbCPU)

        def majBarrePartie(*args):
            barre.update()
        for _ in range(self.args.nbParties):
            res.append(pool.apply_async(effectuerPartie, args=(self.othello, self.args, ignorerErr,), callback=majBarrePartie))
        pool.close()
        pool.join()
        barre.close()
        for i in res:
            resultat.append(i.get())
        for i in resultat:
            temp += i
        return temp

    def entrainerReseau(self, ignorerErr):
        """ Permet d'entraîner le réseau avec les exemples accumulés

        Arguments:
            ignorerErr {bool} -- True si on peut ignorer les erreurs de chargement
        """
        resNeuro = ReseauNeuro(self.othello)
        try:
            resNeuro.chargerReprise(self.args.reprise, 'recent.pth.tar')
        except:
            # if not ignorerErr:
            #     exit("Entraînement: échec du chargement du dernier modèle 'recent.pth.tar'")
            pass

        while len(self.historiqueExemples) > self.args.seuilHistoModeles:
            # print("Taille de l'historique d'exemples:", len(self.historiqueExemples), " => suppression des exemples des anciens modèles")
            self.historiqueExemples.pop(0)
        # On enregistre l'historique dans un fichier
        # /!\ Les exemples sont collectés en utilisant le modèle de l'itération précédente /!\
        self.sauverExemples()

        exemples = []
        for exemple in self.historiqueExemples:
            exemples.extend(exemple)
        shuffle(exemples)

        resNeuro.sauverReprise(self.args.reprise, 'ancien.pth.tar')
        resNeuro.entrainer(exemples)
        resNeuro.sauverReprise(self.args.reprise, 'entrainement.pth.tar')

    def asyncComparaison(self, iteration):
        """ Permet d'effectuer les combats de comparaison entre le dernier modèle
        approuvé et le dernier modèle d'entraînement de manière asynchrone

        Arguments:
            iteration {int} -- N° d'itération
        """
        pool = multiprocessing.Pool(processes=self.args.nbCPU)
        res = []
        resultats = []
        barre = tqdm(total=self.args.combatsComparaison, desc="Comparaison", unit="partie", leave=False)
        barre.set_postfix(cpu=self.args.nbCPU)

        def majBarreCompa(*args):
            barre.update()
        for partie in range(self.args.combatsComparaison):
            res.append(pool.apply_async(comparaison, args=(self.othello, self.args, partie >= int(self.args.combatsComparaison / 2),), callback=majBarreCompa))
        pool.close()
        pool.join()

        for i in res:
            resultats.append(i.get())
        compte = Counter(resultats)
        precVict = compte[1]
        nouvVict = compte[-1]
        nuls = len(resultats) - precVict - nouvVict
        ratio = float(nouvVict + 0.5 * nuls) / (precVict + nouvVict + nuls)
        if precVict + nouvVict > 0 and ratio >= self.args.seuilRatioVictoire:
            self.passerPremierePartie = True
            resNeuro = ReseauNeuro(self.othello)  # Approbation du dernier modèle
            resNeuro.chargerReprise(self.args.reprise, 'entrainement.pth.tar')
            resNeuro.sauverReprise(self.args.reprise, 'recent.pth.tar')
            resNeuro.sauverReprise(self.args.reprise, self.getFichierReprise(iteration))
        barre.close()
        return ratio

    def apprendre(self):
        """ Effectue nbIterations itérations dans nbParties parties en faisant jouer
        le réseau contre lui-même à chaque itération

        Après chaque itération, le réseau neuronal est réentrainé avec des exemples
        des précédentes itérations (dans la limite de seuilExemples).
        Le réseau neuronal combat alors contre l'ancien et le remplace seulement si
        son ratio de victoires est >= au seuilRatioVictoire
        """
        with tqdm(range(self.args.nbIterations), desc="Itération", unit="iteration") as barre:
            for iteration in barre:
                if not self.passerPremierePartie or iteration > 0:  # Si on vient de charger les exemples, pas la peine d'en rechercher d'autres
                    exemples = deque([], maxlen=self.args.seuilExemples)
                    temp = self.asyncEffectuerPartie(not self.passerPremierePartie)
                    exemples += temp
                    self.historiqueExemples.append(exemples)
                self.entrainerReseau(not self.passerPremierePartie)
                ratio = self.asyncComparaison(iteration)
                barre.set_postfix(dernierVD=ratio)
