import torch.nn.functional as F
import torch.nn as nn
import torch
import sys
sys.path.append('..')


class OthReseauNeuro(nn.Module):
    """
    Cette classe est une classe enfant du module nn de PyTorch.
    Les noms de variables et fonctions ne doivent pas être changés car ils sont
    dans la majorité hérités du Module
    """

    def __init__(self, othello, args):
        # Définition des paramètres
        self.dimX, self.dimY = othello.getDimensionsOthellier()
        self.nbActions = othello.getNombreActions()
        self.args = args

        super(OthReseauNeuro, self).__init__()
        # Redéfinition des paramètres hérités du module nn
        self.conv1 = nn.Conv2d(1, args.nbCouches, 3, stride=1, padding=1)
        self.conv2 = nn.Conv2d(args.nbCouches, args.nbCouches, 3, stride=1, padding=1)
        self.conv3 = nn.Conv2d(args.nbCouches, args.nbCouches, 3, stride=1)
        self.conv4 = nn.Conv2d(args.nbCouches, args.nbCouches, 3, stride=1)

        self.bn1 = nn.BatchNorm2d(args.nbCouches)
        self.bn2 = nn.BatchNorm2d(args.nbCouches)
        self.bn3 = nn.BatchNorm2d(args.nbCouches)
        self.bn4 = nn.BatchNorm2d(args.nbCouches)

        self.fc1 = nn.Linear(args.nbCouches * (self.dimX - 4) * (self.dimY - 4), 1024)
        self.fc_bn1 = nn.BatchNorm1d(1024)

        self.fc2 = nn.Linear(1024, 512)
        self.fc_bn2 = nn.BatchNorm1d(512)

        self.fc3 = nn.Linear(512, self.nbActions)

        self.fc4 = nn.Linear(512, 1)

    def forward(self, s):
        """ Redéfinition de la fonction forward du module nn

        Elle permet de définir les opérations d'un pas

        Arguments:
            s {Othellier} -- Othellier canonique
        """
        #                                                                                              s: taillePaquet * dimX * dimY
        s = s.view(-1, 1, self.dimX, self.dimY)                                                         # taillePaquet * 1 * dimX * dimY
        s = F.relu(self.bn1(self.conv1(s)))                                                             # taillePaquet * nbCouches * dimX * dimY
        s = F.relu(self.bn2(self.conv2(s)))                                                             # taillePaquet * nbCouches * dimX * dimY
        s = F.relu(self.bn3(self.conv3(s)))                                                             # taillePaquet * nbCouches * (dimX - 2) * (dimY - 2)
        s = F.relu(self.bn4(self.conv4(s)))                                                             # taillePaquet * nbCouches * (dimX - 4) * (dimY - 4)
        s = s.view(-1, self.args.nbCouches * (self.dimX - 4) * (self.dimY - 4))

        s = F.dropout(F.relu(self.fc_bn1(self.fc1(s))), p=self.args.dropout, training=self.training)    # taillePaquet * 1024
        s = F.dropout(F.relu(self.fc_bn2(self.fc2(s))), p=self.args.dropout, training=self.training)    # taillePaquet * 512

        pi = self.fc3(s)                                                                                # taillePaquet * nbActions
        v = self.fc4(s)                                                                                 # taillePaquet * 1

        return F.log_softmax(pi, dim=1), torch.tanh(v)
