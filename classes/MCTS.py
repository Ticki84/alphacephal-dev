import math
import numpy as np
epsilon = 1e-8  # Pour corriger l'erreur d'approximation sur les floats


class MCTS():
    """ Cette classe contient la MCTS (Recherche Arborescente Monte-Carlo).
    Il est renforcé par l'utilisation du réseau neuronal pour apprendre plus profondément.
    """

    def __init__(self, othello, resNeuro, args):
        self.othello = othello
        self.resNeuro = resNeuro
        self.args = args

        # Les noms de variables sont choisis en anglais pour plus de compréhension des documents principalement anglophones sur AlphaZero
        self.Qsa = {}                   # la récompense attendue en effectuant l'action a depuis l'othellier s
        self.Nsa = {}                   # le nombre de visite de l'action a depuis l'othellier s
        self.Ns = {}                    # le nombre de visite de l'othellier s
        self.Pi = {}                    # vecteur ligne des estimations initiales à choisir telle action depuis l'othellier s, en accord avec le "Policy Network" du réseau neuronal
        # Le "Policy Network" sélectionne les coups, pas forcément optimaux mais à priori plutôt bons

        self.Es = {}                    # l'état de l'othellier s
        self.Vs = {}                    # les coups valides pour l'othellier s

    def getProbaAction(self, othellierCanonique, diviseur=1):
        """ Effectue des simulations du MCTS à partir de l'othellier canonique pour renvoyer les probabilités

        Arguments:
            othellierCanonique {np.array} -- Othellier actuel sous forme canonique

        Keyword Arguments:
            diviseur {int} -- Dénominateur de la puissance des probabilités, 0 ou 1 (default: {1})

        Returns:
            np.array -- Vecteur des estimations Pi où la probabilité de la i-ième action est
                        proportionnelle à Nsa[(s, a)] ** (1. / diviseur)
        """
        for _ in range(self.args.nbSimusMCTS):
            self.rechercher(othellierCanonique)

        s = self.othello.getRepresentation(othellierCanonique)
        nbVisites = [self.Nsa[(s, a)] if (s, a) in self.Nsa else 0 for a in range(self.othello.getNombreActions())]  # Tuple contenant le nombres d'occurences de chacunes des actions

        if diviseur == 0:
            # Si le diviseur de la puissance vaut 0, la proba du plus fréquent tend vers 1, les autres vers 0
            frequent = np.argmax(nbVisites)
            probas = [0] * len(nbVisites)
            probas[frequent] = 1
            return probas

        # Sinon, on peut calculer les probas individuellement
        nbVisites = [x**(1. / diviseur) for x in nbVisites]
        probas = [x / float(sum(nbVisites)) for x in nbVisites]  # On normalise les probabilités
        return probas

    def rechercher(self, othellierCanonique):
        """ Effectue une itération du MCTS

        Cette fonction est appelée récursivement jusqu'à ce que l'état final de l'othellier soit déterminée.
        L'action choisie à chaque récursion est celle qui possède la limite supérieure de confiance maximale,
        par rapport à une MTCS classique, l'UCB1 est remplacé par cette limite.
        Quand un othellier est trouvé, le réseau neuronal est appelé pour retourner le vecteur des estimations
        Pi et une valeur de l'état v, valeurs propagées dans la recherche, autrement dit par rapport à un MCTS
        classique la propagation par retour en arrière est remplacée par le réseau neuronal et on crée les
        branches enfants sans les visiter.
        Dans le cas où l'état de l'othellier est déterminé, le résultat de la partie est renvoyé.

        Arguments:
            othellierCanonique {np.array} -- Othellier actuel sous forme canonique

        Returns:
            float -- Inverse du résultat de la partie compris dans [-1, 1]
        """

        s = self.othello.getRepresentation(othellierCanonique)

        if s not in self.Es:
            # Si l'état de l'othellier n'est pas encore dans la liste Es, on l'ajoute
            self.Es[s] = self.othello.getFinPartie(othellierCanonique, 1)
            self.Es[s] = self.Es[s] if self.Es[s] != -0.5 else -1  # On considère le match nul comme une défaite pour éviter d'en faire
        if self.Es[s] != 0:
            # Si la partie est finie, on renvoie l'inverse du résultat de la partie
            return -self.Es[s]

        if s not in self.Pi:
            # Si l'othellier s n'est pas encore dans le vecteur Pi, on calcule les estimations des actions
            self.Pi[s], v = self.resNeuro.predire(othellierCanonique)
            coups = self.othello.getCoupsValides(othellierCanonique, 1)
            self.Pi[s] = self.Pi[s] * coups  # On masque les coups non valides (on les mets à 0)
            sommePi = np.sum(self.Pi[s])
            if sommePi > 0:
                self.Pi[s] /= sommePi    # On normalise les probabilités (puisqu'on vient d'en masquer)
            else:
                # Si tous les coups sont masqués, on rend tous les coups équiprobables.
                # Il se peut que tous les coups soient masqués si l'architecture de ReseauNeuronal est insuffisante ou sursuffisante.
                # Si on reçoit trop de ces messages, on doit vérifier le code de la classe ReseauNeuronal et/ou le processus d'entraînement
                print("Attention: tous les coups ont été masqué, il faut vérifier la classe ReseauNeuronal et le processus d'entraînement!")
                self.Pi[s] = self.Pi[s] + coups  # Les coups valides valent tous 1
                self.Pi[s] /= np.sum(self.Pi[s])  # On normalise

            self.Vs[s] = coups  # On actualise les coups valides
            self.Ns[s] = 0  # On informe qu'on a pas encore visité s
            return -v  # On renvoie l'inverse du résultat de la partie

        # Si l'othellier est déjà référencé dans le vecteur Pi, on reprend ses coups valides
        coups = self.Vs[s]
        meilleurProba = -float('inf')
        meilleurAction = -1

        # On sélectionne l'action avec la limite supérieure de confiance maximale
        for a in range(self.othello.getNombreActions()):
            if coups[a]:
                # Si l'action est valide
                if (s, a) in self.Qsa:
                    # Si la probabilité de l'action a pour l'othellier s est déjà listée dans la liste des récompenses attendues, on applique la formule
                    proba = self.Qsa[(s, a)] + self.args.cpuct * self.Pi[s][a] * math.sqrt(self.Ns[s]) / (1 + self.Nsa[(s, a)])
                else:
                    # Sinon on applique une version modifiée de la formule
                    proba = self.args.cpuct * self.Pi[s][a] * math.sqrt(self.Ns[s] + epsilon)  # Récompense attendue nulle?

                if proba > meilleurProba:
                    # On actualise la meilleure action actuelle
                    meilleurProba = proba
                    meilleurAction = a

        a = meilleurAction
        # On simule la meilleure action trouvée
        prochainOthellier, prochainJoueur = self.othello.getProchainEtat(othellierCanonique, 1, a)
        prochainOthellier = self.othello.getFormeCanonique(prochainOthellier, prochainJoueur)

        v = self.rechercher(prochainOthellier)  # On appelle récursivement rechercher pour notre nouvel Othellier

        if (s, a) in self.Qsa:
            # On ajoute une visite à l'action a depuis s
            self.Qsa[(s, a)] = (self.Nsa[(s, a)] * self.Qsa[(s, a)] + v) / (self.Nsa[(s, a)] + 1)
            self.Nsa[(s, a)] += 1
        else:
            # On a visité l'action a depuis s pour la première fois
            self.Qsa[(s, a)] = v
            self.Nsa[(s, a)] = 1

        self.Ns[s] += 1  # On ajoute une visite à s
        return -v  # On renvoie l'inverse du résultat de la partie
