# Création de l'interface
from tkinter import*
import threading
import os
import sys
from PIL import Image, ImageTk
from win32api import GetSystemMetrics
from itertools import cycle
# Création des parties
from classes.Othello import Othello
from classes.Arene import Arene
from classes.Joueurs import JoueurAleatoire, JoueurGourmand, JoueurHumain
from classes.ReseauNeuro import ReseauNeuro
from classes.MCTS import MCTS
# Utilitaires
import numpy as np
from classes.utils import dotdict
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
from pygame import mixer


class Interface():
    def __init__(self):
        """ Initialisation de l'interface, définition des paramètres de la fenêtre et création de celle-ci
        """
        self.longueurFen, self.hauteurFen = GetSystemMetrics(0), GetSystemMetrics(1)
        self.globalFont = 'earth orbiter'  # Good Times Rg
        self.globalColor = 'deepskyblue'
        self.globalFond = 'gray7'
        self.globalActive = 'cyan'
        self.globalDisabled = 'deepskyblue4'

        # Barre d'informations à droite de l'Othellier
        self.longueurInfos = self.longueurFen * 0.3
        self.centreInfos = self.longueurFen - self.longueurInfos / 2
        margeGD = self.longueurInfos / 2
        self.centreGInfos = self.centreInfos - margeGD / 2
        self.centreDInfos = self.centreInfos + margeGD / 2
        self.margeTitre = self.hauteurFen * 0.11
        self.margeDetail = self.hauteurFen * 0.07
        self.tailleTitre = 30
        self.tailleDetail = 28

        self.hauteurLogo = 100
        self.longueurLogo = self.hauteurLogo * 6
        self.margeLogoHaut = self.hauteurLogo / 2 + self.hauteurLogo * 0.3
        self.margeLogoBas = self.margeLogoHaut * 1.5
        self.margeYCase = self.margeLogoHaut + self.margeLogoBas

        self.tailleOptions = 24

        self.cyc_tab = cycle(['Options', 'Partie', 'Coups'])
        next(self.cyc_tab)
        self.premLancement = True

        self.listeCoups = []
        self.listeOthelliers = []

        # Création de la fenêtre principale
        self.fen = Tk()
        self.fen.title("Alphacephal")
        self.fen.state('zoomed')
        self.fen.protocol("WM_DELETE_WINDOW", self.quitter)

        # Création des variables membres contenant les images
        chemin = os.path.abspath(os.path.dirname(sys.argv[0]))
        imgLogo = Image.open(chemin + '/images/alphacephal.png')
        imgLogo = imgLogo.resize((self.longueurLogo, self.hauteurLogo), Image.ANTIALIAS)
        self.logo = ImageTk.PhotoImage(imgLogo)
        imgFeuArtifice = Image.open(chemin + '/images/feuartifice.png')
        imgFeuArtifice = imgFeuArtifice.resize((self.longueurFen, self.hauteurFen), Image.ANTIALIAS)
        self.feuArtifice = ImageTk.PhotoImage(imgFeuArtifice)

        self.case = None
        self.canevas = None
        self.threadArene = None

        self.afficherOptions()
        self.afficherCoups()

        self.fen.mainloop()

    def afficherCoups(self):
        """ Création et affichage de l'onglet des coups
        """
        self.tabCoups = Frame(self.fen, width=self.longueurFen, height=self.hauteurFen, bg=self.globalFond)
        self.varCoups = StringVar(self.fen, self.listeCoups)
        self.listboxCoups = Listbox(self.tabCoups, width=60, height=25, font=(self.globalFont, self.tailleOptions), activestyle='none', foreground=self.globalColor, background='gray9', listvariable=self.varCoups,
                                    highlightbackground='gray6', highlightcolor='gray6', bd=0, highlightthickness=2, justify=CENTER, selectforeground=self.globalActive, selectbackground='gray15', selectmode='single')
        self.listboxCoups.grid(row=0)

        def fct_allerA(val):
            if not self.partieFinie and not (self.relancee == 0 and self.listboxCoups.curselection()[0] == self.listboxCoups.size() - 1) and (self.attInput or (self.relancee > 0 and self.listboxCoups.curselection()[0] != self.relancee - 1)):
                self.attInput = False
                self.undoPrevision()
                self.previ = [[], 1, {}]
                self.arene.othellier = self.listeOthelliers[self.listboxCoups.curselection()[0]][0]
                self.arene.joueurActuel = self.listeOthelliers[self.listboxCoups.curselection()[0]][1]
                self.mettreAJour(self.arene.othellier, self.arene.joueurActuel)
                self.relancee = self.listboxCoups.curselection()[0] + 1
                self.canevas.itemconfig(self.coupG, text=self.listeOthelliers[self.relancee - 1][2], fill=self.globalColor)
                self.canevas.itemconfig(self.coupD, text=self.listeOthelliers[self.relancee - 1][3], fill=self.globalColor)
                self.canevas.itemconfig(self.statut, text="Cliquez pour reprendre", state=NORMAL)
                while next(self.cyc_tab) != 'Partie':
                    pass
                Misc.lift(self.canevas)
                self.fen.wait_variable(self.case)

        self.lbl_allerA = Label(self.tabCoups, text='Aller à', font=(self.globalFont, self.tailleOptions), foreground=self.globalColor, background=self.globalFond)
        self.lbl_allerA.grid(row=1)
        self.lbl_allerA.bind('<Button-1>', fct_allerA)
        self.lbl_allerA.bind("<Enter>", lambda ev: self.lbl_allerA.config(foreground=self.globalActive if not self.partieFinie and not (self.relancee == 0 and self.listboxCoups.curselection()[0] == self.listboxCoups.size() - 1) and (self.attInput or (self.relancee > 0 and self.listboxCoups.curselection()[0] != self.relancee - 1)) else self.globalDisabled))
        self.lbl_allerA.bind("<Leave>", lambda ev: self.lbl_allerA.config(foreground=self.globalColor if not self.partieFinie and not (self.relancee == 0 and self.listboxCoups.curselection()[0] == self.listboxCoups.size() - 1) and (self.attInput or (self.relancee > 0 and self.listboxCoups.curselection()[0] != self.relancee - 1)) else self.globalDisabled))
        self.listboxCoups.bind('<<ListboxSelect>>', lambda ev: self.lbl_allerA.config(foreground=self.globalColor if not self.partieFinie and not (self.relancee == 0 and self.listboxCoups.curselection()[0] == self.listboxCoups.size() - 1) and (self.attInput or (self.relancee > 0 and self.listboxCoups.curselection()[0] != self.relancee - 1)) else self.globalDisabled))

        self.tabCoups.grid_rowconfigure(0, weight=1)
        self.tabCoups.grid_rowconfigure(1, weight=1)
        self.tabCoups.grid_columnconfigure(0, weight=1)
        self.tabCoups.pack()
        self.tabCoups.place(relwidth=1, relheight=1)
        Misc.lift(self.options)

    def afficherOptions(self):
        """ Création et affichage des options
        """
        self.options = Frame(self.fen, width=self.longueurFen, height=self.hauteurFen, bg=self.globalFond)

        def entree(lbl, enable=True):
            lbl.config(foreground=self.globalActive if enable else self.globalDisabled)
            lbl.hov = True

        def sortie(lbl, enable=True, edit=False):
            lbl.config(foreground=(self.globalActive if edit else self.globalColor) if enable else self.globalDisabled)
            lbl.hov = False

        row = 0

        def fct_joueur1(val):
            if not self.edit_joueur1 and self.premLancement:
                self.onEntree(None)
                self.var_joueur1.set("|")
                self.edit_joueur1 = True

        self.var_joueur1 = StringVar(self.fen, 'Alphacephal')
        self.sauv_joueur1 = self.var_joueur1.get()
        self.defaut_joueur1 = self.var_joueur1.get()
        self.edit_joueur1 = False
        self.lbl_joueur1 = Label(self.options, textvariable=self.var_joueur1, font=(self.globalFont, self.tailleOptions), foreground=self.globalColor, background=self.globalFond)
        self.lbl_joueur1.grid(column=2, row=row, columnspan=4)
        self.lbl_joueur1.bind('<Button-1>', fct_joueur1)
        self.lbl_joueur1.bind("<Enter>", lambda ev: entree(self.lbl_joueur1, self.premLancement))
        self.lbl_joueur1.bind("<Leave>", lambda ev: sortie(self.lbl_joueur1, self.premLancement, self.edit_joueur1))

        def fct_joueur2(val):
            if not self.edit_joueur2 and self.premLancement:
                self.onEntree(None)
                self.var_joueur2.set("|")
                self.edit_joueur2 = True

        self.var_joueur2 = StringVar(self.fen, 'Joueur 2')
        self.sauv_joueur2 = self.var_joueur2.get()
        self.defaut_joueur2 = self.var_joueur2.get()
        self.edit_joueur2 = False
        self.lbl_joueur2 = Label(self.options, textvariable=self.var_joueur2, font=(self.globalFont, self.tailleOptions), foreground=self.globalColor, background=self.globalFond)
        self.lbl_joueur2.grid(column=6, row=row, columnspan=4)
        self.lbl_joueur2.bind('<Button-1>', fct_joueur2)
        self.lbl_joueur2.bind("<Enter>", lambda ev: entree(self.lbl_joueur2, self.premLancement))
        self.lbl_joueur2.bind("<Leave>", lambda ev: sortie(self.lbl_joueur2, self.premLancement, self.edit_joueur2))

        row += 1

        def fct_type1(val):
            if self.premLancement:
                self.var_type1.set(next(self.cyc_type1))
                if self.edit_mcts1 or self.edit_cpuct1:
                    self.onEntree(None)
                self.lbl_mcts1.config(foreground=(self.globalActive if self.lbl_mcts1.hov else self.globalColor) if self.var_type1.get() == 'Alphacephal' and self.premLancement else self.globalDisabled)
                self.lbl_cpuct1.config(foreground=(self.globalActive if self.lbl_cpuct1.hov else self.globalColor) if self.var_type1.get() == 'Alphacephal' and self.premLancement else self.globalDisabled)

        self.cyc_type1 = cycle(['Alphacephal', 'Aléatoire', 'Gourmand', 'Humain'])
        self.var_type1 = StringVar(self.fen, next(self.cyc_type1))
        self.defaut_type1 = self.var_type1.get()
        lbl_type1 = Label(self.options, textvariable=self.var_type1, font=(self.globalFont, self.tailleOptions), foreground=self.globalColor, background=self.globalFond)
        lbl_type1.grid(column=2, row=row, columnspan=4)
        lbl_type1.bind('<Button-1>', fct_type1)
        lbl_type1.bind("<Enter>", lambda ev: entree(lbl_type1, self.premLancement))
        lbl_type1.bind("<Leave>", lambda ev: sortie(lbl_type1, self.premLancement))

        def fct_type2(val):
            if self.premLancement:
                self.var_type2.set(next(self.cyc_type2))
                if self.edit_mcts2 or self.edit_cpuct2:
                    self.onEntree(None)
                self.lbl_mcts2.config(foreground=(self.globalActive if self.lbl_mcts2.hov else self.globalColor) if self.var_type2.get() == 'Alphacephal' and self.premLancement else self.globalDisabled)
                self.lbl_cpuct2.config(foreground=(self.globalActive if self.lbl_cpuct2.hov else self.globalColor) if self.var_type2.get() == 'Alphacephal' and self.premLancement else self.globalDisabled)

        self.cyc_type2 = cycle(['Humain', 'Alphacephal', 'Aléatoire', 'Gourmand'])
        self.var_type2 = StringVar(self.fen, next(self.cyc_type2))
        self.defaut_type2 = self.var_type2.get()
        lbl_type2 = Label(self.options, textvariable=self.var_type2, font=(self.globalFont, self.tailleOptions), foreground=self.globalColor, background=self.globalFond)
        lbl_type2.grid(column=6, row=row, columnspan=4)
        lbl_type2.bind('<Button-1>', fct_type2)
        lbl_type2.bind("<Enter>", lambda ev: entree(lbl_type2, self.premLancement))
        lbl_type2.bind("<Leave>", lambda ev: sortie(lbl_type2, self.premLancement))

        row += 1

        def fct_mcts1(val):
            if not self.edit_mcts1 and self.var_type1.get() == 'Alphacephal' and self.premLancement:
                self.onEntree(None)
                self.edit_mcts1 = True
                self.int_mcts1.set(-1)

        self.int_mcts1 = IntVar(self.fen, 100)
        var_mcts1 = StringVar(self.fen)
        self.edit_mcts1 = False
        self.int_mcts1.trace('w', lambda *args: var_mcts1.set('MTCS: ' + ((str(self.int_mcts1.get()) + ('#' if self.edit_mcts1 else '')) if self.int_mcts1.get() >= 0 else '#')))
        self.int_mcts1.set(100)
        self.sauv_mcts1 = self.int_mcts1.get()
        self.defaut_mcts1 = self.int_mcts1.get()
        self.lbl_mcts1 = Label(self.options, textvariable=var_mcts1, font=(self.globalFont, self.tailleOptions), foreground=self.globalColor, background=self.globalFond)
        self.lbl_mcts1.grid(column=2, row=row, columnspan=2)
        self.lbl_mcts1.hov = False
        self.lbl_mcts1.bind('<Button-1>', fct_mcts1)
        self.lbl_mcts1.bind("<Enter>", lambda ev: entree(self.lbl_mcts1, self.var_type1.get() == 'Alphacephal' and self.premLancement))
        self.lbl_mcts1.bind("<Leave>", lambda ev: sortie(self.lbl_mcts1, self.var_type1.get() == 'Alphacephal' and self.premLancement, self.edit_mcts1))

        def fct_cpuct1(val):
            if not self.edit_cpuct1 and self.var_type1.get() == 'Alphacephal' and self.premLancement:
                self.onEntree(None)
                self.float_cpuct1.set(-1)
                self.edit_cpuct1 = True

        self.float_cpuct1 = DoubleVar(self.fen, 1)
        var_cpuct1 = StringVar(self.fen)
        self.float_cpuct1.trace('w', lambda *args: var_cpuct1.set('Degré d\'expl.: ' + (str(self.float_cpuct1.get()) if self.float_cpuct1.get() >= 0 else '#.#')))
        self.float_cpuct1.set(1)
        self.sauv_cpuct1 = self.float_cpuct1.get()
        self.defaut_cpuct1 = self.float_cpuct1.get()
        self.edit_cpuct1 = False
        self.lbl_cpuct1 = Label(self.options, textvariable=var_cpuct1, font=(self.globalFont, self.tailleOptions), foreground=self.globalColor, background=self.globalFond)
        self.lbl_cpuct1.grid(column=4, row=row, columnspan=2)
        self.lbl_cpuct1.hov = False
        self.lbl_cpuct1.bind('<Button-1>', fct_cpuct1)
        self.lbl_cpuct1.bind("<Enter>", lambda ev: entree(self.lbl_cpuct1, self.var_type1.get() == 'Alphacephal' and self.premLancement))
        self.lbl_cpuct1.bind("<Leave>", lambda ev: sortie(self.lbl_cpuct1, self.var_type1.get() == 'Alphacephal' and self.premLancement, self.edit_cpuct1))

        def fct_mcts2(val):
            if not self.edit_mcts2 and self.var_type2.get() == 'Alphacephal' and self.premLancement:
                self.onEntree(None)
                self.edit_mcts2 = True
                self.int_mcts2.set(-1)
        self.int_mcts2 = IntVar(self.fen, 100)
        var_mcts2 = StringVar(self.fen)
        self.edit_mcts2 = False
        self.int_mcts2.trace('w', lambda *args: var_mcts2.set('MTCS: ' + ((str(self.int_mcts2.get()) + ('#' if self.edit_mcts2 else '')) if self.int_mcts2.get() >= 0 else '#')))
        self.int_mcts2.set(100)
        self.sauv_mcts2 = self.int_mcts2.get()
        self.defaut_mcts2 = self.int_mcts2.get()
        self.lbl_mcts2 = Label(self.options, textvariable=var_mcts2, font=(self.globalFont, self.tailleOptions), foreground=self.globalDisabled, background=self.globalFond)
        self.lbl_mcts2.grid(column=6, row=row, columnspan=2)
        self.lbl_mcts2.hov = False
        self.lbl_mcts2.bind('<Button-1>', fct_mcts2)
        self.lbl_mcts2.bind("<Enter>", lambda ev: entree(self.lbl_mcts2, self.var_type2.get() == 'Alphacephal' and self.premLancement))
        self.lbl_mcts2.bind("<Leave>", lambda ev: sortie(self.lbl_mcts2, self.var_type2.get() == 'Alphacephal' and self.premLancement, self.edit_mcts2))

        def fct_cpuct2(val):
            if not self.edit_cpuct2 and self.var_type2.get() == 'Alphacephal' and self.premLancement:
                self.onEntree(None)
                self.float_cpuct2.set(-1)
                self.edit_cpuct2 = True
        self.float_cpuct2 = DoubleVar(self.fen, 1)
        var_cpuct2 = StringVar(self.fen)
        self.float_cpuct2.trace('w', lambda *args: var_cpuct2.set('Degré d\'expl.: ' + (str(self.float_cpuct2.get()) if self.float_cpuct2.get() >= 0 else '#.#')))
        self.float_cpuct2.set(1)
        self.sauv_cpuct2 = self.float_cpuct2.get()
        self.defaut_cpuct2 = self.float_cpuct2.get()
        self.edit_cpuct2 = False
        self.lbl_cpuct2 = Label(self.options, textvariable=var_cpuct2, font=(self.globalFont, self.tailleOptions), foreground=self.globalDisabled, background=self.globalFond)
        self.lbl_cpuct2.grid(column=8, row=row, columnspan=2)
        self.lbl_cpuct2.hov = False
        self.lbl_cpuct2.bind('<Button-1>', fct_cpuct2)
        self.lbl_cpuct2.bind("<Enter>", lambda ev: entree(self.lbl_cpuct2, self.var_type2.get() == 'Alphacephal' and self.premLancement))
        self.lbl_cpuct2.bind("<Leave>", lambda ev: sortie(self.lbl_cpuct2, self.var_type2.get() == 'Alphacephal' and self.premLancement, self.edit_cpuct2))

        row += 1

        def fct_comm(val, comm):
            if self.premLancement:
                self.int_comm = comm
                lbl_comm1.config(text='Commence' if comm == 1 else 'Ne commence pas')
                lbl_comm2.config(text='Commence' if comm == 2 else 'Ne commence pas')

        self.int_comm = 1
        self.defaut_comm = self.int_comm
        lbl_comm1 = Label(self.options, text='Commence', font=(self.globalFont, self.tailleOptions), foreground=self.globalColor, background=self.globalFond)
        lbl_comm1.grid(column=2, row=row, columnspan=4)
        lbl_comm1.bind('<Button-1>', lambda val: fct_comm(val, 1))
        lbl_comm1.bind("<Enter>", lambda ev: entree(lbl_comm1, self.premLancement))
        lbl_comm1.bind("<Leave>", lambda ev: sortie(lbl_comm1, self.premLancement))
        lbl_comm2 = Label(self.options, text='Ne commence pas', font=(self.globalFont, self.tailleOptions), foreground=self.globalColor, background=self.globalFond)
        lbl_comm2.grid(column=6, row=row, columnspan=4)
        lbl_comm2.bind('<Button-1>', lambda val: fct_comm(val, 2))
        lbl_comm2.bind("<Enter>", lambda ev: entree(lbl_comm2, self.premLancement))
        lbl_comm2.bind("<Leave>", lambda ev: sortie(lbl_comm2, self.premLancement))

        row += 1

        def fct_dim(val):
            if self.premLancement:
                self.int_dim.set(next(self.cyc_dim))

        self.cyc_dim = cycle([6, 8])
        var_dim = StringVar(self.fen)
        self.int_dim = IntVar(self.fen, 6)
        self.int_dim.trace('w', lambda *args: var_dim.set('Dimensions: ' + str(self.int_dim.get()) + 'x' + str(self.int_dim.get())))
        self.int_dim.set(next(self.cyc_dim))
        self.defaut_dim = self.int_dim.get()
        lbl_dim = Label(self.options, textvariable=var_dim, font=(self.globalFont, self.tailleOptions), foreground=self.globalColor, background=self.globalFond)
        lbl_dim.grid(column=3, row=row, columnspan=2)
        lbl_dim.bind('<Button-1>', fct_dim)
        lbl_dim.bind("<Enter>", lambda ev: entree(lbl_dim, self.premLancement))
        lbl_dim.bind("<Leave>", lambda ev: sortie(lbl_dim, self.premLancement))

        def fct_pts(val):
            if self.premLancement:
                self.bool_pts = not self.bool_pts
                lbl_pts.config(text='Prévisu.: oui' if self.bool_pts else 'Prévisu.: non')

        self.bool_pts = True
        self.defaut_pts = self.bool_pts
        lbl_pts = Label(self.options, text='Prévisu.: oui', font=(self.globalFont, self.tailleOptions), foreground=self.globalColor, background=self.globalFond)
        lbl_pts.grid(column=5, row=row, columnspan=2)
        lbl_pts.bind('<Button-1>', fct_pts)
        lbl_pts.bind("<Enter>", lambda ev: entree(lbl_pts, self.premLancement))
        lbl_pts.bind("<Leave>", lambda ev: sortie(lbl_pts, self.premLancement))

        def fct_tpsMin(val):
            if not self.edit_tpsMin:
                self.onEntree(None)
                self.float_tpsMin.set(-1)
                self.edit_tpsMin = True

        self.float_tpsMin = DoubleVar(self.fen, 0)
        var_tpsMin = StringVar(self.fen)
        self.float_tpsMin.trace('w', lambda *args: var_tpsMin.set('Temps min.: ' + (str(self.float_tpsMin.get()) if self.float_tpsMin.get() >= 0 else '#.#') + 's'))
        self.float_tpsMin.set(0)
        self.sauv_tpsMin = self.float_tpsMin.get()
        self.defaut_tpsMin = self.float_tpsMin.get()
        self.edit_tpsMin = False
        self.lbl_tpsMin = Label(self.options, textvariable=var_tpsMin, font=(self.globalFont, self.tailleOptions), foreground=self.globalColor, background=self.globalFond)
        self.lbl_tpsMin.grid(column=7, row=row, columnspan=2)
        self.lbl_tpsMin.bind('<Button-1>', fct_tpsMin)
        self.lbl_tpsMin.bind("<Enter>", lambda ev: entree(self.lbl_tpsMin))
        self.lbl_tpsMin.bind("<Leave>", lambda ev: sortie(self.lbl_tpsMin, True, self.edit_tpsMin))

        row += 1

        def fct_gui(val):
            if self.premLancement:
                self.bool_gui = not self.bool_gui
                lbl_gui.config(text='GUI: oui' if self.bool_gui else 'GUI: non')

        self.bool_gui = True
        self.defaut_gui = self.bool_gui
        lbl_gui = Label(self.options, text='GUI: oui', font=(self.globalFont, self.tailleOptions), foreground=self.globalColor, background=self.globalFond)
        lbl_gui.grid(column=3, row=row, columnspan=2)
        lbl_gui.hov = False
        lbl_gui.bind('<Button-1>', fct_gui)
        lbl_gui.bind("<Enter>", lambda ev: entree(lbl_gui, self.premLancement))
        lbl_gui.bind("<Leave>", lambda ev: sortie(lbl_gui, self.premLancement))

        def fct_musique(val):
            self.bool_musique = not self.bool_musique
            if self.bool_musique:
                mixer.music.play(-1)
            else:
                mixer.music.stop()
            lbl_musique.config(text='Musique: oui' if self.bool_musique else 'Musique: non')
            if self.edit_volume and not self.bool_musique:
                self.onEntree(None)
            self.lbl_volume.config(foreground=(self.globalActive if self.lbl_volume.hov else self.globalColor) if self.bool_musique else self.globalDisabled)

        mixer.init()
        mixer.music.load("Nightcall.ogg")
        mixer.music.set_volume(0.6)
        self.bool_musique = False
        self.defaut_musique = self.bool_musique
        lbl_musique = Label(self.options, text='Musique: non', font=(self.globalFont, self.tailleOptions), foreground=self.globalColor, background=self.globalFond)
        lbl_musique.grid(column=5, row=row, columnspan=2)
        lbl_musique.bind('<Button-1>', fct_musique)
        lbl_musique.bind("<Enter>", lambda ev: entree(lbl_musique))
        lbl_musique.bind("<Leave>", lambda ev: sortie(lbl_musique))

        def fct_volume(val):
            if not self.edit_volume and self.bool_musique:
                self.onEntree(None)
                self.edit_volume = True
                self.int_volume.set(-1)

        self.int_volume = IntVar(self.fen, 60)
        var_volume = StringVar(self.fen)
        self.edit_volume = False
        self.int_volume.trace('w', lambda *args: var_volume.set('Volume: ' + ((str(self.int_volume.get()) + ('#' * (3 - len(str(self.int_volume.get()))) if self.edit_volume else '')) if self.int_volume.get() >= 0 else '###') + '%'))
        self.int_volume.set(60)
        self.sauv_volume = self.int_volume.get()
        self.defaut_volume = self.int_volume.get()
        self.lbl_volume = Label(self.options, textvariable=var_volume, font=(self.globalFont, self.tailleOptions), foreground=self.globalDisabled, background=self.globalFond)
        self.lbl_volume.grid(column=7, row=row, columnspan=2)
        self.lbl_volume.hov = False
        self.lbl_volume.bind('<Button-1>', fct_volume)
        self.lbl_volume.bind("<Enter>", lambda ev: entree(self.lbl_volume, self.bool_musique))
        self.lbl_volume.bind("<Leave>", lambda ev: sortie(self.lbl_volume, self.bool_musique, self.edit_volume))

        row += 2

        def fct_lancer(val):
            self.onEntree(None)
            self.editOptions = False
            if self.premLancement:
                self.lbl_joueur1.config(foreground=self.globalDisabled)
                self.lbl_joueur2.config(foreground=self.globalDisabled)
                lbl_type1.config(foreground=self.globalDisabled)
                lbl_type2.config(foreground=self.globalDisabled)
                self.lbl_mcts1.config(foreground=self.globalDisabled)
                self.lbl_mcts2.config(foreground=self.globalDisabled)
                self.lbl_cpuct1.config(foreground=self.globalDisabled)
                self.lbl_cpuct2.config(foreground=self.globalDisabled)
                lbl_comm1.config(foreground=self.globalDisabled)
                lbl_comm2.config(foreground=self.globalDisabled)
                lbl_dim.config(foreground=self.globalDisabled)
                lbl_pts.config(foreground=self.globalDisabled)
                lbl_gui.config(foreground=self.globalDisabled)
                self.defaut_joueur1 = self.sauv_joueur1
                self.defaut_joueur2 = self.sauv_joueur2
                self.defaut_type1 = self.var_type1.get()
                self.defaut_type2 = self.var_type2.get()
                self.defaut_mcts1 = self.sauv_mcts1
                self.defaut_mcts2 = self.sauv_mcts2
                self.defaut_cpuct1 = self.sauv_cpuct1
                self.defaut_cpuct2 = self.sauv_cpuct2
                self.defaut_comm = self.int_comm
                self.defaut_dim = self.int_dim.get()
                self.defaut_pts = self.bool_pts
                self.defaut_tpsMin = self.sauv_tpsMin
                self.defaut_gui = self.bool_gui
                self.defaut_musique = self.bool_musique
                self.defaut_volume = self.int_volume.get()
                self.dernCase = (self.defaut_dim, self.defaut_dim)
                self.premLancement = False
                lbl_lancer.config(text="Rejouer")
                self.lancer()
            else:
                self.attInput = False
                self.undoPrevision()
                self.previ = [[], 1, {}]
                self.arene.othellier = self.othello.getOthellierInitial()
                self.arene.joueurActuel = 1
                self.listeCoups = ["Début de la partie"]
                self.listeOthelliers = [(np.copy(self.arene.othellier), self.arene.joueurActuel, '~~', '~~')]
                self.mettreAJour(self.arene.othellier, self.arene.joueurActuel)
                self.relancee = 1
                self.canevas.itemconfig(self.coupG, text='~~', fill=self.globalColor)
                self.canevas.itemconfig(self.coupD, text='~~', fill=self.globalColor)
                self.canevas.itemconfig(self.statut, text="Cliquez pour reprendre", state=NORMAL)
                while next(self.cyc_tab) != 'Partie':
                    pass
                Misc.lift(self.canevas)
                self.fen.wait_variable(self.case)
        lbl_lancer = Label(self.options, text='Lancer', font=(self.globalFont, self.tailleOptions), foreground=self.globalColor, background=self.globalFond)
        lbl_lancer.grid(column=0, row=row, columnspan=4)
        lbl_lancer.bind("<Enter>", lambda ev: entree(lbl_lancer))
        lbl_lancer.bind("<Leave>", lambda ev: sortie(lbl_lancer))
        lbl_lancer.bind('<Button-1>', fct_lancer)

        def fct_reini(val):
            self.onEchap(None)
            self.var_joueur1.set(self.defaut_joueur1)
            self.sauv_joueur1 = self.defaut_joueur1
            self.var_joueur2.set(self.defaut_joueur2)
            self.sauv_joueur2 = self.defaut_joueur2
            while next(self.cyc_type1) != self.defaut_type1:
                pass
            while next(self.cyc_type2) != self.defaut_type2:
                pass
            self.var_type1.set(self.defaut_type1)
            self.var_type2.set(self.defaut_type2)
            self.int_mcts1.set(self.defaut_mcts1)
            self.sauv_mcts1 = self.defaut_mcts1
            self.int_mcts2.set(self.defaut_mcts2)
            self.sauv_mcts2 = self.defaut_mcts2
            self.float_cpuct1.set(self.defaut_cpuct1)
            self.sauv_cpuct1 = self.defaut_cpuct1
            self.float_cpuct2.set(self.defaut_cpuct2)
            self.sauv_cpuct2 = self.defaut_cpuct2
            self.lbl_mcts1.config(foreground=(self.globalActive if self.lbl_mcts1.hov else self.globalColor) if self.var_type1.get() == 'Alphacephal' and self.premLancement else self.globalDisabled)
            self.lbl_mcts2.config(foreground=(self.globalActive if self.lbl_mcts2.hov else self.globalColor) if self.var_type2.get() == 'Alphacephal' and self.premLancement else self.globalDisabled)
            self.lbl_cpuct1.config(foreground=(self.globalActive if self.lbl_cpuct1.hov else self.globalColor) if self.var_type1.get() == 'Alphacephal' and self.premLancement else self.globalDisabled)
            self.lbl_cpuct2.config(foreground=(self.globalActive if self.lbl_cpuct2.hov else self.globalColor) if self.var_type2.get() == 'Alphacephal' and self.premLancement else self.globalDisabled)
            fct_comm(None, self.defaut_comm)
            while next(self.cyc_dim) != self.defaut_dim:
                pass
            self.int_dim.set(self.defaut_dim)
            if self.bool_pts != self.defaut_pts:
                fct_pts(None)
            self.float_tpsMin.set(self.defaut_tpsMin)
            self.sauv_tpsMin = self.defaut_tpsMin
            self.bool_gui = self.defaut_gui
            if self.bool_musique != self.defaut_musique:
                fct_musique(None)
            self.int_volume.set(self.defaut_volume)
            self.sauv_volume = self.defaut_volume
            mixer.music.set_volume(self.sauv_volume / 100.)
            self.lbl_volume.config(foreground=(self.globalActive if self.lbl_volume.hov else self.globalColor) if self.bool_musique else self.globalDisabled)
        lbl_reini = Label(self.options, text='Réinitialiser', font=(self.globalFont, self.tailleOptions), foreground=self.globalColor, background=self.globalFond)
        lbl_reini.grid(column=8, row=row, columnspan=4)
        lbl_reini.bind('<Button-1>', fct_reini)
        lbl_reini.bind("<Enter>", lambda ev: entree(lbl_reini))
        lbl_reini.bind("<Leave>", lambda ev: sortie(lbl_reini))

        for r in range(row + 1):
            self.options.grid_rowconfigure(r, weight=1)
        for c in range(12):
            self.options.grid_columnconfigure(c, weight=1)
        self.options.pack()
        self.options.place(relwidth=1, relheight=1)
        for numpad in range(10):
            self.fen.bind(str(numpad), self.onChiffre)
        self.fen.bind('<Return>', self.onEntree)
        self.fen.bind('<BackSpace>', self.onRetour)
        self.fen.bind('<Delete>', self.onRetour)
        self.fen.bind('<Escape>', self.onEchap)
        self.fen.bind('<Key>', self.onLettre)
        self.fen.bind('<Tab>', self.onTab)
        self.editOptions = True

    def lancer(self):
        """ Lance l'arène (et des parties indéfiniement)
        """
        # On (supprime puis) crée les (anciennes) variables
        if self.threadArene:
            self.arene.interface = None
            self.threadArene.Terminated = True
        if not self.case:
            self.case = IntVar(self.fen, value=-1)
        else:
            self.case.set(-1)
        if self.canevas:
            del self.canevas

        # Redéfinition des paramètres
        self.othello = Othello(self.defaut_dim)
        self.nomJ1 = self.defaut_joueur1
        self.nomJ2 = self.defaut_joueur2
        self.afficherPts = self.defaut_pts
        self.tpsMinTour = self.defaut_tpsMin

        self.msgGagnant = False                         # True si le message du gagnant est affiché à l'écran
        self.lancee = False                             # True si la partie est lancée
        self.relancee = 0                               # > 0 si la partie vient d'être relancée à un état précédent, 0 sinon
        self.passTour = False                           # True si il y a passage de tour
        self.partieFinie = False                        # True si la partie est finie
        self.attInput = False                           # True si on attend l'input de l'utilisateur sur l'interface
        self.premPartie = False                         # True si la première partie a été lancé
        self.msgInversion = False                       # True si le message d'inversion des joueurs est affiché à l'écran
        self.inverse = False                            # True si l'inversion des joueurs a déjà été effectué
        self.previ = [[], 1, {}]                        # Stocke [OthellierActuel, JoueurActuel, {action: OthellierPrevision}]
        self.estG = True                                # True si c'est la partie gauche des informations qui est à mettre à jour
        self.entree = '-1'                              # Stocke l'entrée utilisateur (sous forme de Str(int))

        if self.defaut_gui:
            # Redéfinition des longueurs et tailles
            self.tailleNom = int(min(self.longueurInfos / 2 * 0.85 / len(self.nomJ1), self.longueurInfos / 2 * 0.85 / len(self.nomJ2), self.longueurInfos / 2 * 0.85 / 6))
            self.longueur = int((self.hauteurFen - self.margeYCase) * 0.8 / self.othello.n)
            self.margeXCase = (self.longueurFen - self.longueurInfos) / 2 - self.othello.n * self.longueur / 2
            # Indices en haut et à gauche de l'Othellier
            self.margeIndices = self.longueur * 0.35
            self.tailleIndices = int(self.longueur * 0.3)

            # Redimensionnement des images
            chemin = os.path.abspath(os.path.dirname(sys.argv[0]))
            imgN1 = Image.open(chemin + '/images/n1.png')
            imgN1 = imgN1.resize((self.longueur, self.longueur), Image.ANTIALIAS)
            self.n1 = ImageTk.PhotoImage(imgN1)
            imgN2 = Image.open(chemin + '/images/n2.png')
            imgN2 = imgN2.resize((self.longueur, self.longueur), Image.ANTIALIAS)
            self.n2 = ImageTk.PhotoImage(imgN2)
            imgN3 = Image.open(chemin + '/images/n3.png')
            imgN3 = imgN3.resize((self.longueur, self.longueur), Image.ANTIALIAS)
            self.n3 = ImageTk.PhotoImage(imgN3)
            imgB1 = Image.open(chemin + '/images/b1.png')
            imgB1 = imgB1.resize((self.longueur, self.longueur), Image.ANTIALIAS)
            self.b1 = ImageTk.PhotoImage(imgB1)
            imgB2 = Image.open(chemin + '/images/b2.png')
            imgB2 = imgB2.resize((self.longueur, self.longueur), Image.ANTIALIAS)
            self.b2 = ImageTk.PhotoImage(imgB2)
            imgB3 = Image.open(chemin + '/images/b3.png')
            imgB3 = imgB3.resize((self.longueur, self.longueur), Image.ANTIALIAS)
            self.b3 = ImageTk.PhotoImage(imgB3)
            tailleEtat = int(self.longueur * 0.4)
            imgEteint = Image.open(chemin + '/images/eteint.png')
            imgEteint = imgEteint.resize((tailleEtat, tailleEtat), Image.ANTIALIAS)
            self.eteint = ImageTk.PhotoImage(imgEteint)
            imgAllume = Image.open(chemin + '/images/allume.png')
            imgAllume = imgAllume.resize((tailleEtat, tailleEtat), Image.ANTIALIAS)
            self.allume = ImageTk.PhotoImage(imgAllume)

            # Création du canevas
            self.canevas = Canvas(self.fen, width=self.longueurFen, height=self.hauteurFen, bg=self.globalFond)
            # Affichage du statut en bas de l'othellier
            self.statut = self.canevas.create_text((self.longueurFen - self.longueurInfos) / 2, self.hauteurFen * 0.88, font=(self.globalFont, self.tailleTitre), text="Veuillez patienter...", fill=self.globalColor, anchor=CENTER)
            # Affichage du fond des infos
            self.canevas.create_rectangle(self.longueurFen - self.longueurInfos,
                                          0,
                                          self.longueurFen,
                                          self.hauteurFen,
                                          fill='grey9', outline='gray11', width=2)

            self.listePions = []
            self.listeCases = []
            self.dernCase = (self.othello.n, self.othello.n)

            # Affichage des indices
            for i in range(self.othello.n):
                self.canevas.create_text(self.margeXCase + self.longueur * (i + 0.5), self.margeYCase - self.margeIndices, font=(self.globalFont, self.tailleIndices), fill=self.globalColor, text=i)
                self.canevas.create_text(self.margeXCase - self.margeIndices, self.margeYCase + self.longueur * (i + 0.5), font=(self.globalFont, self.tailleIndices), fill=self.globalColor, text=i)
            # Affichage du logo
            self.canevas.create_image((self.longueurFen - self.longueurInfos) / 2, self.margeLogoHaut, image=self.logo, anchor=CENTER)
            # Affichage des cases de l'othellier
            for i in range(self.othello.n):
                lig = []
                lig2 = []
                for j in range(self.othello.n):
                    lig.append([None, None])
                    lig2.append(None)
                self.listePions.append(lig)
                self.listeCases.append(lig2)
            for i in range(self.othello.n):
                for j in range(self.othello.n):
                    self.listeCases[j][i] = (self.canevas.create_rectangle(self.margeXCase + self.longueur * i,
                                                                           self.margeYCase + self.longueur * j,
                                                                           self.margeXCase + self.longueur * (i + 1),
                                                                           self.margeYCase + self.longueur * (j + 1),
                                                                           fill='deepskyblue', width=3, outline=self.canevas.cget("bg")))

            # Affichage de la barre d'Infos
            precMargeY = self.margeTitre * 0.8
            self.icoG = self.canevas.create_image(self.centreGInfos, precMargeY, image=self.n1, anchor=CENTER)
            self.etatG = self.canevas.create_image(self.centreGInfos + self.longueur * 0.35, precMargeY - self.longueur * 0.35, image=self.eteint, anchor=CENTER)
            self.nomG = self.canevas.create_text(self.centreGInfos, precMargeY + self.longueur * 0.7, font=(self.globalFont, self.tailleNom), fill=self.globalColor, text=self.nomJ1, anchor=CENTER)
            self.icoD = self.canevas.create_image(self.centreDInfos, precMargeY, image=self.b1, anchor=CENTER)
            self.etatD = self.canevas.create_image(self.centreDInfos + self.longueur * 0.35, precMargeY - self.longueur * 0.35, image=self.eteint, anchor=CENTER)
            self.nomD = self.canevas.create_text(self.centreDInfos, precMargeY + self.longueur * 0.7, font=(self.globalFont, self.tailleNom), fill=self.globalColor, text=self.nomJ2, anchor=CENTER)
            precMargeY += self.margeTitre + self.longueur * 0.5
            self.canevas.create_text(self.centreInfos, precMargeY, font=(self.globalFont, self.tailleTitre), fill=self.globalColor, text="SCORE")
            precMargeY += self.margeDetail
            self.scoreG = self.canevas.create_text(self.centreGInfos, precMargeY, font=(self.globalFont, self.tailleDetail), fill=self.globalColor, text="2")
            self.scoreD = self.canevas.create_text(self.centreDInfos, precMargeY, font=(self.globalFont, self.tailleDetail), fill=self.globalColor, text="2")
            precMargeY += self.margeTitre
            self.canevas.create_text(self.centreInfos, precMargeY, font=(self.globalFont, self.tailleTitre), fill=self.globalColor, text="VICTOIRES")
            precMargeY += self.margeDetail
            self.victoiresG = self.canevas.create_text(self.centreGInfos, precMargeY, font=(self.globalFont, self.tailleDetail), fill=self.globalColor, text="0")
            self.victoiresD = self.canevas.create_text(self.centreDInfos, precMargeY, font=(self.globalFont, self.tailleDetail), fill=self.globalColor, text="0")
            precMargeY += self.margeTitre
            self.canevas.create_text(self.centreInfos, precMargeY, font=(self.globalFont, self.tailleTitre), fill=self.globalColor, text="RATIO V/D")
            precMargeY += self.margeDetail
            self.ratioG = self.canevas.create_text(self.centreGInfos, precMargeY, font=(self.globalFont, self.tailleDetail), fill=self.globalColor, text="~~~~")
            self.ratioD = self.canevas.create_text(self.centreDInfos, precMargeY, font=(self.globalFont, self.tailleDetail), fill=self.globalColor, text="~~~~")
            precMargeY += self.margeTitre
            self.canevas.create_text(self.centreInfos, precMargeY, font=(self.globalFont, self.tailleTitre), fill=self.globalColor, text="COUP")
            precMargeY += self.margeDetail
            self.margeYCoup = precMargeY
            self.coupG = self.canevas.create_text(self.centreGInfos, precMargeY, font=(self.globalFont, self.tailleDetail), fill=self.globalColor, text='~~')
            self.coupD = self.canevas.create_text(self.centreDInfos, precMargeY, font=(self.globalFont, self.tailleDetail), fill=self.globalColor, text='~~')

            # Message de fin de partie
            self.objGagnant = []
            self.objGagnant.append(self.canevas.create_image(self.longueurFen / 2, self.hauteurFen / 2, image=self.feuArtifice, anchor=CENTER, state=HIDDEN))
            self.objGagnant.append(self.canevas.create_text(self.longueurFen / 2, self.hauteurFen / 5, text="Le gagnant est", anchor=S, font=(self.globalFont, self.hauteurFen // 14), fill="whitesmoke", state=HIDDEN))
            self.objGagnant.append(self.canevas.create_text(self.longueurFen / 2, self.hauteurFen / 2, text="Joueur ~", anchor=S, font=(self.globalFont, self.hauteurFen // 10), fill="whitesmoke", state=HIDDEN))
            self.objGagnant.append(self.canevas.create_text(self.longueurFen / 2, self.hauteurFen * 4 / 5, text="Cliquez pour reprendre", anchor=S, font=(self.globalFont, self.hauteurFen // 20), fill="whitesmoke", state=HIDDEN))

            self.canevas.pack()
            self.canevas.place(x=-2, y=-2)
            self.canevas.bind('<Button 1>', self.clique)
            self.canevas.bind('<Motion>', self.bouge)
            while next(self.cyc_tab) != 'Partie':
                pass
            Misc.lift(self.canevas)
        else:
            self.fen.destroy()

        # Joueur aléatoire: joue aléatoirement une des cases valides
        jAleatoire = JoueurAleatoire(self.othello).jouer
        # Joueur gourmand: joue toujours la case où il obtiendra le plus de points
        jGourmand = JoueurGourmand(self.othello).jouer
        # Joueur humain: permet de saisir manuellement les cases où jouer
        jHumain = JoueurHumain(self.othello).jouer
        # Joueur humain interface: permet de cliquer sur les cases où jouer
        jHumainInterface = self.jouer
        j1 = jHumainInterface if self.defaut_type1 == "Humain" and self.defaut_gui else (jHumain if self.defaut_type1 == "Humain" else (jAleatoire if self.defaut_type1 == "Aléatoire" else (jGourmand if self.defaut_type1 == 'Gourmand' else None)))
        if not j1:
            # Joueur de type réseaux neuronaux
            neuro1 = ReseauNeuro(self.othello)
            neuro1.chargerReprise('./modeles/', (str(self.othello.n) + 'x100_recent.pth.tar'))
            args1 = dotdict({'nbSimusMCTS': self.defaut_mcts1, 'cpuct': self.defaut_cpuct1})
            mcts1 = MCTS(self.othello, neuro1, args1)

            def j1(proba): return np.argmax(mcts1.getProbaAction(proba, 0))
        j2 = jHumainInterface if self.defaut_type2 == "Humain" and self.defaut_gui else (jHumain if self.defaut_type2 == "Humain" else (jAleatoire if self.defaut_type2 == "Aléatoire" else (jGourmand if self.defaut_type2 == 'Gourmand' else None)))
        if not j2:
            # Joueur de type réseaux neuronaux
            neuro2 = ReseauNeuro(self.othello)
            neuro2.chargerReprise('./modeles/', (str(self.othello.n) + 'x100_recent.pth.tar'))
            args2 = dotdict({'nbSimusMCTS': self.defaut_mcts2, 'cpuct': self.defaut_cpuct2})
            mcts2 = MCTS(self.othello, neuro2, args2)

            def j2(proba): return np.argmax(mcts2.getProbaAction(proba, 0))

        self.arene = Arene(j1, j2, self.othello, afficher=Othello.afficher, interface=self if self.defaut_gui else None)
        if not self.defaut_gui:
            self.arene.nomJ1 = self.nomJ1
            self.arene.nomJ2 = self.nomJ2
        if self.defaut_comm == 2:
            self.inverse = True
            self.arene.inverse = self.inverse
            self.arene.joueur1, self.arene.joueur2 = self.arene.joueur2, self.arene.joueur1
            self.arene.nomJ1, self.arene.nomJ2 = self.arene.nomJ2, self.arene.nomJ1
            self.canevas.itemconfig(self.icoG, image=self.b1 if self.inverse else self.n1)
            self.canevas.itemconfig(self.icoD, image=self.n1 if self.inverse else self.b1)
        self.threadArene = threading.Thread(target=self.arene.jouerParties, args=(not self.defaut_gui,))
        self.threadArene.start()

    def quitter(self):
        """ Permet de quitter l'interface

        Le programme continue, si possible, son éxecution
        """
        if self.threadArene:
            self.arene.interface = None
            self.threadArene.Terminated = True
        self.fen.destroy()
        # if self.case:
        #     self.relancee = 1
        #     self.case.set(-1)  # Pour quitter le wait_variable dans jouer()
        sys.exit(0)

    def jouer(self, othellier):
        """ Fonction de jeu du joueur humain (pour jouer par l'interface graphique)

        Avec vérifications de la validité de la case saisie

        Arguments:
            othellier {Othellier} -- Othellier actuel

        Returns:
            int -- Case où le joueur souhaite jouer
        """
        coups = self.othello.getCoupsValides(othellier, 1)
        if coups[-1] == 1:
            return len(coups) - 1  # Si le joueur ne peut pas jouer, on lui fait passer son tour
        nCase = -1
        if self.afficherPts:
            self.previ[2] = {}
            for case in range(len(coups)):
                if coups[case] == 1:
                    previOthellier, _ = self.othello.getProchainEtat(othellier, 1, case)
                    self.previ[2][case] = previOthellier
        self.canevas.itemconfig(self.coupG if self.estG else self.coupD, text='##', fill=self.globalActive)
        self.attInput = True
        while nCase < 0 or nCase >= len(coups) or not coups[nCase]:
            self.fen.wait_variable(self.case)
            nCase = self.case.get()
            if self.relancee > 0:
                self.listeOthelliers = self.listeOthelliers[:self.relancee]
                self.listeCoups = self.listeCoups[:self.relancee]
                self.relancee = 0
                nCase = self.othello.n**2 + 1  # Si la partie vient d'être relancée à un certain stade
                break
        self.attInput = False
        self.canevas.itemconfig(self.coupG if self.estG else self.coupD, fill=self.globalColor)
        self.case.set(-1)
        return nCase

    def mettreAJour(self, othellier, joueur):
        """ Permet de mettre à jour l'interface graphique

        Change la couleur des pions retournés, affiche le nouveau pion, supprime les
        anciennes prédictions et affiche les nouvelles

        Arguments:
            othellier {Othellier} -- Othellier actuel
            joueur {int} -- 1: noir, -1: blanc
        """
        self.previ[0] = othellier
        self.previ[1] = joueur
        self.estG = self.previ[1] * (-1 if self.inverse else 1) == 1
        self.canevas.itemconfig(self.etatG, image=self.allume if self.estG else self.eteint)
        self.canevas.itemconfig(self.etatD, image=self.allume if not self.estG else self.eteint)
        scores = self.othello.getScores(othellier)
        self.canevas.itemconfig(self.scoreG, text=scores[0] if not self.inverse else scores[1])
        self.canevas.itemconfig(self.scoreD, text=scores[1] if not self.inverse else scores[0])
        self.coupsValides = self.othello.getCoupsValides(othellier, joueur)
        for y in range(self.othello.n):
            for x in range(self.othello.n):
                case = othellier[y][x]
                if case != 0:  # Si la case doit contenir un pion
                    if self.listePions[y][x][1] != case:  # On vérifie que l'image est la bonne
                        self.canevas.delete(self.listePions[y][x][0])
                        self.listePions[y][x][0] = self.canevas.create_image(self.margeXCase + self.longueur * x,
                                                                             self.margeYCase + self.longueur * y,
                                                                             image=self.n1 if case == 1 else self.b1, anchor=NW)
                        self.listePions[y][x][1] = case
                elif self.listePions[y][x][0]:  # Si la case vaut 0 et qu'elle contient une image, supprime aussi les prévisions
                    self.canevas.delete(self.listePions[y][x][0])
                    self.listePions[y][x] = [None, None]
                if self.coupsValides[self.othello.n * y + x]:  # On ajoute les coups valides
                    self.listePions[y][x][0] = self.canevas.create_image(self.margeXCase + self.longueur * x,
                                                                         self.margeYCase + self.longueur * y,
                                                                         image=self.n2 if joueur == 1 else self.b2, anchor=NW)
                    self.listePions[y][x][1] = 0

    def afficherPrevision(self, action):
        """ Permet d'afficher la prévisualisation visuelle

        Arguments:
             {(int, int)} -- Case (y, x) où effectuer l'action
        """
        if self.attInput:
            y, x = action
            if self.dernCase != (y, x):
                self.undoPrevision()
                if self.coupsValides[self.othello.n * y + x]:
                    if self.othello.n * y + x in self.previ[2] or not self.afficherPts:
                        self.entree = str(y) + str(x)
                        self.canevas.itemconfig(self.coupG if self.estG else self.coupD, text=self.entree)
                        self.dernCase = (y, x)
                        self.canevas.itemconfig(self.listeCases[y][x], fill=self.globalActive)
                        if self.afficherPts:
                            for j in range(self.othello.n):
                                for i in range(self.othello.n):
                                    case = self.previ[2][self.othello.n * y + x][j][i]
                                    if case != 0:  # Si la case dans la prévision contient un pion
                                        if not self.listePions[j][i][1] or self.previ[1] * self.listePions[j][i][1] != case:  # On regarde si l'image a changé avec la prévisualisation
                                            self.canevas.delete(self.listePions[j][i][0])
                                            self.listePions[j][i][0] = self.canevas.create_image(self.margeXCase + self.longueur * i,
                                                                                                 self.margeYCase + self.longueur * j,
                                                                                                 image=self.n3 if self.previ[1] == 1 else self.b3, anchor=NW)
                                            self.listePions[y][x][1] = 0
                else:
                    self.entree = '-1'
                    self.canevas.itemconfig(self.coupG if self.estG else self.coupD, text='##')

    def undoPrevision(self):
        """ Permet de faire un retour en arrière sur la prévisualisation visuelle
        """
        y, x = self.dernCase[0], self.dernCase[1]
        if self.dernCase != (self.defaut_dim, self.defaut_dim):
            self.canevas.itemconfig(self.listeCases[y][x], fill=self.globalColor)
            if self.afficherPts and self.previ[0] != []:
                for j in range(self.othello.n):
                    for i in range(self.othello.n):
                        case = self.previ[0][j][i]
                        if case != 0:  # Si la case doit contenir un pion
                            if self.listePions[j][i][1] != self.previ[1] * (1 if not self.inverse else -1) * case:  # On vérifie que l'image est la bonne
                                self.canevas.delete(self.listePions[j][i][0])
                                self.listePions[j][i][0] = self.canevas.create_image(self.margeXCase + self.longueur * i,
                                                                                     self.margeYCase + self.longueur * j,
                                                                                     image=self.n1 if case == 1 else self.b1, anchor=NW)
                                self.listePions[j][i][1] = case
                        elif self.listePions[j][i][0]:  # Si la case vaut 0 et qu'elle contient une image, supprime aussi les prévisions
                            self.canevas.delete(self.listePions[j][i][0])
                            self.listePions[j][i] = [None, None]
                        if self.coupsValides[self.othello.n * j + i]:  # On ajoute les coups valides
                            self.listePions[j][i][0] = self.canevas.create_image(self.margeXCase + self.longueur * i,
                                                                                 self.margeYCase + self.longueur * j,
                                                                                 image=self.n2 if self.previ[1] == 1 else self.b2, anchor=NW)
                            self.listePions[j][i][1] = 0
            self.dernCase = (self.othello.n, self.othello.n)

    def clique(self, ev):
        """ Gère le clique utilisateur

        Arguments:
            ev {event} -- Evènement du clique
        """
        if not self.msgGagnant and self.lancee and not self.msgInversion and self.relancee <= 0 and not self.passTour:
            y, x = (ev.y - self.margeYCase) // self.longueur, (ev.x - self.margeXCase) // self.longueur
            self.case.set(self.othello.n * y + x)
            if self.dernCase != (self.othello.n, self.othello.n):
                self.canevas.itemconfig(self.listeCases[self.dernCase[0]][self.dernCase[1]], fill=self.globalColor)
                self.undoPrevision()
                self.entree = '-1'
                self.canevas.itemconfig(self.coupG if self.estG else self.coupD, text='##')
        elif self.msgGagnant:
            self.lancee = False
            self.msgGagnant = False
            self.canevas.itemconfig(self.statut, state=NORMAL)
            for obj in self.objGagnant:
                self.canevas.itemconfig(obj, state=HIDDEN)
        elif self.relancee > 0:
            self.canevas.itemconfig(self.statut, text="Cliquez pour continuer", state=HIDDEN)
            self.case.set(-1)
        elif self.passTour:
            self.passTour = False
            self.canevas.itemconfig(self.statut, text="Cliquez pour continuer", state=HIDDEN)
            self.case.set(-1)
        elif not self.lancee:
            self.lancee = True
            self.canevas.itemconfig(self.statut, state=HIDDEN)
            self.case.set(-1)
        elif self.msgInversion:
            self.msgInversion = False
            self.canevas.itemconfig(self.statut, text="Cliquez pour continuer", state=HIDDEN)
            self.case.set(-1)

    def bouge(self, ev):
        """ Gère les évènements lors du déplacement de la souris

        Pour afficher les coups possibles

        Arguments:
            ev {event} -- Evènement du déplacement
        """
        if self.attInput:
            y, x = int((ev.y - self.margeYCase) // self.longueur), int((ev.x - self.margeXCase) // self.longueur)
            if 0 <= x < self.othello.n and 0 <= y < self.othello.n and self.attInput:
                self.afficherPrevision((y, x))

    def finPartie(self, othellier, joueurActuel, resultat):
        """ Gère la fin de partie

        Affichage du message du gagnant, mise à jour des infos sur l'interface, attente d'un clique

        Arguments:
            othellier {Othellier} -- Othellier actuel
            joueurActuel {int} -- 1: noir, -1: blanc
            resultat {int} -- 1: victoire joueur 1, -1: victoire joueur 2, -0.5: match nul
        """
        self.mettreAJour(othellier, joueurActuel)
        self.partieFinie = True
        self.msgGagnant = True
        self.canevas.itemconfig(self.objGagnant[2], text=(self.nomJ1 if resultat * (-1 if self.inverse else 1) == 1 else self.nomJ2) if resultat != -0.5 else "Match nul")
        for obj in self.objGagnant:
            self.canevas.tag_raise(obj)
            self.canevas.itemconfig(obj, state=NORMAL)
        self.canevas.itemconfig(self.objGagnant[1], state=HIDDEN if resultat == -0.5 else NORMAL)
        self.canevas.itemconfig(self.etatG, image=self.eteint)
        self.canevas.itemconfig(self.etatD, image=self.eteint)
        victoiresG = int(self.canevas.itemcget(self.victoiresG, 'text'))
        victoiresD = int(self.canevas.itemcget(self.victoiresD, 'text'))
        if resultat * (-1 if self.inverse else 1) == 1:
            victoiresG += 1
        elif resultat != -0.5:
            victoiresD += 1
        self.canevas.itemconfig(self.victoiresG, text=str(victoiresG))
        self.canevas.itemconfig(self.victoiresD, text=str(victoiresD))
        self.canevas.itemconfig(self.ratioG, text=str(round(victoiresG / (victoiresG + victoiresD), 2)) if victoiresG + victoiresD > 0 else '~~~~')
        self.canevas.itemconfig(self.ratioD, text=str(round(victoiresD / (victoiresG + victoiresD), 2)) if victoiresG + victoiresD > 0 else '~~~~')
        self.fen.wait_variable(self.case)

    def debutPartie(self, othellier, joueur):
        """ Gère le début de partie

        Arguments:
            othellier {Othellier} -- Othellier actuel
            joueur {int} -- Joueur actuel
        """
        self.partieFinie = False
        self.canevas.itemconfig(self.coupG, text='~~')
        self.canevas.itemconfig(self.coupD, text='~~')
        if not self.premPartie:
            self.premPartie = True
            self.mettreAJour(othellier, joueur)
            self.canevas.itemconfig(self.statut, text="Cliquez pour continuer")
            self.fen.wait_variable(self.case)

    def inversionJoueurs(self):
        """ Gère l'inversion des couleurs lorsque la moitié des parties a été effectué

        Affichage d'un message, échange des scores
        """
        self.inverse = not self.inverse
        self.msgInversion = True
        self.canevas.itemconfig(self.statut, text="Inversion des couleurs", state=NORMAL)
        self.canevas.itemconfig(self.icoG, image=self.b1 if self.inverse else self.n1)
        self.canevas.itemconfig(self.icoD, image=self.n1 if self.inverse else self.b1)
        self.fen.wait_variable(self.case)

    def onChiffre(self, ev):
        """ Gère les touches numériques

        Arguments:
            ev {event} -- Evènement
        """
        chiffre = ev.char
        if self.editOptions:
            chiffre = int(chiffre)
            if self.edit_joueur1:
                if len(self.var_joueur1.get()) < 17:
                    self.var_joueur1.set(self.var_joueur1.get()[:-1] + str(chiffre) + '|')
            elif self.edit_joueur2:
                if len(self.var_joueur2.get()) < 17:
                    self.var_joueur2.set(self.var_joueur2.get()[:-1] + str(chiffre) + '|')
            elif self.edit_tpsMin:
                if self.float_tpsMin.get() < 0:
                    self.float_tpsMin.set(int(chiffre))
                else:
                    self.float_tpsMin.set(int(self.float_tpsMin.get()) + float(chiffre) * 0.1)
            elif self.edit_mcts1:
                if self.int_mcts1.get() >= 0:
                    self.int_mcts1.set(self.int_mcts1.get() * 10 + chiffre)
                elif chiffre > 0:
                    self.int_mcts1.set(chiffre)
            elif self.edit_mcts2:
                if self.int_mcts2.get() >= 0:
                    self.int_mcts2.set(self.int_mcts2.get() * 10 + chiffre)
                elif chiffre > 0:
                    self.int_mcts2.set(chiffre)
            elif self.edit_cpuct1:
                if self.float_cpuct1.get() < 0:
                    self.float_cpuct1.set(int(chiffre))
                else:
                    self.float_cpuct1.set(int(self.float_cpuct1.get()) + float(chiffre) * 0.1)
            elif self.edit_cpuct2:
                if self.float_cpuct2.get() < 0:
                    self.float_cpuct2.set(int(chiffre))
                else:
                    self.float_cpuct2.set(int(self.float_cpuct2.get()) + float(chiffre) * 0.1)
            elif self.edit_volume:
                if self.int_volume.get() >= 0:
                    self.int_volume.set(min(self.int_volume.get() * 10 + chiffre, 100))
                elif chiffre > 0:
                    self.int_volume.set(chiffre)
        elif self.attInput and int(chiffre) < self.othello.n:
            if int(self.entree) < self.othello.n:
                if int(self.entree) >= 0:
                    entree = self.entree + chiffre
                    x, y = int(entree[1]), int(entree[0])
                    if self.coupsValides[self.othello.n * y + x]:
                        self.entree += chiffre
                        self.afficherPrevision((y, x))
                else:
                    self.entree = chiffre
                    self.undoPrevision()
            else:
                self.entree = chiffre
                self.undoPrevision()
            self.canevas.itemconfig(self.coupG if self.estG else self.coupD, text=(self.entree + '#' * (2 - len(self.entree))) if int(self.entree) >= 0 else '##')

    def onEntree(self, ev):
        """ Gère la touche Entrée

        Arguments:
            ev {event} -- Evènement
        """
        if self.editOptions:
            if self.edit_joueur1:
                tmp = self.var_joueur1.get()[:-1]
                if tmp == '':
                    self.onEchap(ev)
                    return
                if tmp[-1] == ' ':
                    tmp = tmp[:-1]
                self.var_joueur1.set(tmp)
                self.sauv_joueur1 = tmp
                self.lbl_joueur1.config(foreground=self.globalColor if not self.lbl_joueur1.hov else self.globalActive)
                self.edit_joueur1 = False
            elif self.edit_joueur2:
                tmp = self.var_joueur2.get()[:-1]
                if tmp == '':
                    self.onEchap(ev)
                    return
                if tmp[-1] == ' ':
                    tmp = tmp[:-1]
                self.var_joueur2.set(tmp)
                self.sauv_joueur2 = tmp
                self.lbl_joueur2.config(foreground=self.globalColor if not self.lbl_joueur2.hov else self.globalActive)
                self.edit_joueur2 = False
            elif self.edit_tpsMin:
                if self.float_tpsMin.get() < 0:
                    self.onEchap(ev)
                    return
                self.sauv_tpsMin = self.float_tpsMin.get()
                self.lbl_tpsMin.config(foreground=self.globalColor if not self.lbl_tpsMin.hov else self.globalActive)
                self.edit_tpsMin = False
            elif self.edit_mcts1:
                if self.int_mcts1.get() < 1:
                    self.onEchap(ev)
                    return
                self.sauv_mcts1 = self.int_mcts1.get()
                self.lbl_mcts1.config(foreground=self.globalColor if not self.lbl_mcts1.hov else self.globalActive)
                self.edit_mcts1 = False
                self.int_mcts1.set(self.int_mcts1.get())
            elif self.edit_mcts2:
                if self.int_mcts2.get() < 1:
                    self.onEchap(ev)
                    return
                self.sauv_mcts2 = self.int_mcts2.get()
                self.lbl_mcts2.config(foreground=self.globalColor if not self.lbl_mcts2.hov else self.globalActive)
                self.edit_mcts2 = False
                self.int_mcts2.set(self.int_mcts2.get())
            elif self.edit_cpuct1:
                if self.float_cpuct1.get() <= 0:
                    self.onEchap(ev)
                    return
                self.sauv_cpuct1 = self.float_cpuct1.get()
                self.lbl_cpuct1.config(foreground=self.globalColor if not self.lbl_cpuct1.hov else self.globalActive)
                self.edit_cpuct1 = False
            elif self.edit_cpuct2:
                if self.float_cpuct2.get() <= 0:
                    self.onEchap(ev)
                    return
                self.sauv_cpuct2 = self.float_cpuct2.get()
                self.lbl_cpuct2.config(foreground=self.globalColor if not self.lbl_cpuct2.hov else self.globalActive)
                self.edit_cpuct2 = False
            elif self.edit_volume:
                if self.int_volume.get() <= 0:
                    self.onEchap(ev)
                    return
                self.sauv_volume = self.int_volume.get()
                self.lbl_volume.config(foreground=self.globalColor if not self.lbl_volume.hov else self.globalActive)
                self.edit_volume = False
                self.int_volume.set(self.int_volume.get())
                mixer.music.set_volume(self.sauv_volume / 100.)
        elif self.attInput:
            x, y = -1, -1
            if len(self.entree) == 2 and int(self.entree) >= 0:
                x, y = int(self.entree[1]), int(self.entree[0])
            if x >= 0 and y >= 0:
                assert self.coupsValides[self.othello.n * y + x]
                self.entree = '-1'
                self.case.set(self.othello.n * y + x)
                if self.dernCase != (self.othello.n, self.othello.n):
                    self.canevas.itemconfig(self.listeCases[self.dernCase[0]][self.dernCase[1]], fill=self.globalColor)
                    self.dernCase = (self.othello.n, self.othello.n)

    def onRetour(self, ev):
        """ Gère la touche Retour arr.

        Arguments:
            ev {event} -- Evènement
        """
        if self.editOptions:
            if self.edit_joueur1:
                if self.var_joueur1.get() != '|':
                    self.var_joueur1.set(self.var_joueur1.get()[:-2] + '|')
            elif self.edit_joueur2:
                if self.var_joueur2.get() != '|':
                    self.var_joueur2.set(self.var_joueur2.get()[:-2] + '|')
            elif self.edit_tpsMin:
                if self.float_tpsMin.get() >= 0:
                    self.float_tpsMin.set(int(self.float_tpsMin.get()) if not self.float_tpsMin.get().is_integer() else -1)
            elif self.edit_mcts1:
                if self.int_mcts1.get() >= 0:
                    self.int_mcts1.set(int(self.int_mcts1.get() / 10) if int(self.int_mcts1.get()) >= 10 else -1)
            elif self.edit_mcts2:
                if self.int_mcts2.get() >= 0:
                    self.int_mcts2.set(int(self.int_mcts2.get() / 10) if int(self.int_mcts2.get()) >= 10 else -1)
            elif self.edit_cpuct1:
                if self.float_cpuct1.get() >= 0:
                    self.float_cpuct1.set(int(self.float_cpuct1.get()) if not self.float_cpuct1.get().is_integer() else -1)
            elif self.edit_cpuct2:
                if self.float_cpuct2.get() >= 0:
                    self.float_cpuct2.set(int(self.float_cpuct2.get()) if not self.float_cpuct2.get().is_integer() else -1)
            elif self.edit_volume:
                if self.int_volume.get() >= 0:
                    self.int_volume.set(int(self.int_volume.get() / 10) if int(self.int_volume.get()) >= 10 else -1)
        elif self.attInput:
            self.undoPrevision()
            if self.entree == '-1':
                self.onEchap(None)
            else:
                self.entree = self.entree[:-1]
                self.entree = self.entree if self.entree != '' else '-1'
                self.canevas.itemconfig(self.coupG if self.estG else self.coupD, text=(self.entree + '#' * (2 - len(self.entree))) if int(self.entree) >= 0 else '##')

    def onEchap(self, ev):
        """ Gère la touche Echap

        Arguments:
            ev {event} -- Evènement
        """
        if self.editOptions:
            if self.edit_joueur1:
                self.var_joueur1.set(self.sauv_joueur1)
                self.lbl_joueur1.config(foreground=self.globalColor if not self.lbl_joueur1.hov else self.globalActive)
                self.edit_joueur1 = False
            elif self.edit_joueur2:
                self.var_joueur2.set(self.sauv_joueur2)
                self.lbl_joueur2.config(foreground=self.globalColor if not self.lbl_joueur2.hov else self.globalActive)
                self.edit_joueur2 = False
            elif self.edit_tpsMin:
                self.float_tpsMin.set(self.sauv_tpsMin)
                self.lbl_tpsMin.config(foreground=self.globalColor if not self.lbl_tpsMin.hov else self.globalActive)
                self.edit_tpsMin = False
            elif self.edit_mcts1:
                self.lbl_mcts1.config(foreground=self.globalColor if not self.lbl_mcts1.hov else self.globalActive)
                self.edit_mcts1 = False
                self.int_mcts1.set(self.sauv_mcts1)
            elif self.edit_mcts2:
                self.lbl_mcts2.config(foreground=self.globalColor if not self.lbl_mcts2.hov else self.globalActive)
                self.edit_mcts2 = False
                self.int_mcts2.set(self.sauv_mcts2)
            elif self.edit_cpuct1:
                self.float_cpuct1.set(self.sauv_cpuct1)
                self.lbl_cpuct1.config(foreground=self.globalColor if not self.lbl_cpuct1.hov else self.globalActive)
                self.edit_cpuct1 = False
            elif self.edit_cpuct2:
                self.float_cpuct2.set(self.sauv_cpuct2)
                self.lbl_cpuct2.config(foreground=self.globalColor if not self.lbl_cpuct2.hov else self.globalActive)
                self.edit_cpuct2 = False
            elif self.edit_volume:
                self.lbl_volume.config(foreground=self.globalColor if not self.lbl_volume.hov else self.globalActive)
                self.edit_volume = False
                self.int_volume.set(self.sauv_volume)
        elif self.attInput:
            self.entree = '-1'
            self.undoPrevision()
            self.canevas.itemconfig(self.coupG if self.estG else self.coupD, text='##')

    def onLettre(self, ev):
        """ Gère les touches alphabétiques

        Arguments:
            ev {event} -- Evènement
        """
        if ev.char.isalpha() or ev.char == ' ':
            if self.editOptions:
                if self.edit_joueur1:
                    if len(self.var_joueur1.get()) < 17 and not ((self.var_joueur1.get() == '|' or self.var_joueur1.get()[-2] == ' ') and ev.char == ' '):
                        self.var_joueur1.set(self.var_joueur1.get()[:-1] + ev.char + '|')
                elif self.edit_joueur2:
                    if len(self.var_joueur2.get()) < 17 and not ((self.var_joueur2.get() == '|' or self.var_joueur2.get()[-2] == ' ') and ev.char == ' '):
                        self.var_joueur2.set(self.var_joueur2.get()[:-1] + ev.char + '|')

    def onTab(self, ev):
        """ Gère la touche tabulation

        Arguments:
            ev {event} -- Evènement
        """
        if not self.premLancement and not self.msgGagnant and self.defaut_gui:
            tab = next(self.cyc_tab)
            self.editOptions = tab == 'Options'
            if tab == 'Options':
                self.defaut_tpsMin = self.float_tpsMin.get()
                self.defaut_musique = self.bool_musique
                self.defaut_volume = self.int_volume.get()
                Misc.lift(self.options)
            elif tab == 'Coups':
                self.varCoups.set(self.listeCoups)
                self.listboxCoups.selection_clear(0, "end")
                self.listboxCoups.select_set(self.relancee - 1 if self.relancee > 0 else "end")
                self.listboxCoups.yview(self.relancee - 1 if self.relancee > 0 else "end")
                self.lbl_allerA.config(foreground=self.globalDisabled)
                Misc.lift(self.tabCoups)
            else:
                self.tpsMinTour = self.float_tpsMin.get()
                Misc.lift(self.canevas)
