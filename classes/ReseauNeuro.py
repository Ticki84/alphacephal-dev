import torch.optim as optim
import torch
from classes.OthReseauNeuro import OthReseauNeuro
from classes.utils import dotdict, Moyenne
from tqdm import tqdm_notebook as tqdm
import os
import numpy as np
import sys
import time
sys.path.append('../../')


# Paramètres de l'optimisation d'Adam
# Voir https://machinelearningmastery.com/adam-optimization-algorithm-for-deep-learning/ pour les recommendations
args = dotdict({
    'periodes': 10,                             # Nombre de périodes (càd d'optimisations d'Adam à effectuer)
    'lr': 0.001,                                # Taux d'appentissage (ajustement automatique par l'optimiseur)
    'dropout': 0.3,                             # Taux d'abandon (permet d'éviter de saturer le réseau neuronal)
    'taillePaquet': 64,                         # Taille des paquets d'échantillons utilisés pour nourrir les neurones (diminue la fiabilité si trop élevé)
    'nbCouches': 512,                           # Nombre de couches du réseau (augmente l'espace disque nécessaire au stockage)
    'cuda': torch.cuda.is_available(),          # True si la carte graphique est disponible pour CUDA
})


class ReseauNeuro():
    """ Classe du réseau neuronal résiduel
    Il a pour but principal de généraliser le savoir acquis par MCTS
    """

    def __init__(self, othello):
        self.resNeuro = OthReseauNeuro(othello, args)

        self.dimX, self.dimY = othello.getDimensionsOthellier()
        self.nbActions = othello.getNombreActions()

        if args.cuda:
            self.resNeuro.cuda()

    def entrainer(self, exemples):
        """ Cette fonction entraîne le réseau neuronal en utilisant les exemples
        accumulés puis optimise (entraîne) le réseau avec l'optimisation d'Adam

        Arguments:
            exemples {(othellierCanonique, Pi, v)} -- Liste d'exemples où Pi est le vecteur des estimations donné par
                                                      la MCTS pour chaque action possible de l'othellier (càd le nombre
                                                      de visites de l'action par la MCTS sur le nombre total de visites),
                                                      v la valeur associée (de 1 victoire assurée, à -1 défaite assurée)
        """
        optimiseur = optim.Adam(self.resNeuro.parameters())

        for _ in tqdm(range(args.periodes), desc="Entraînement", unit="optim", leave=False):
            # On passe le réseau en mode entraînement
            self.resNeuro.train()

            tpsDonnees = Moyenne()
            tpsCalcul = Moyenne()
            pertesPi = Moyenne()
            pertesV = Moyenne()
            fin = time.time()

            with tqdm(range(int(len(exemples) / args.taillePaquet)), desc="Traitement", unit="paquets", leave=False) as barre:
                for _0 in barre:
                    idsPaquet = np.random.randint(len(exemples), size=args.taillePaquet)
                    othelliers, Pis, vs = list(zip(*[exemples[i] for i in idsPaquet]))
                    othelliers = torch.FloatTensor(np.array(othelliers).astype(np.float64))
                    PisCible = torch.FloatTensor(np.array(Pis))
                    vsCible = torch.FloatTensor(np.array(vs).astype(np.float64))

                    # Prédictions
                    if args.cuda:
                        othelliers, PisCible, vsCible = othelliers.contiguous().cuda(), PisCible.contiguous().cuda(), vsCible.contiguous().cuda()

                    # Mesure du temps de chargement des données
                    tpsDonnees.update(time.time() - fin)

                    # Calcule les sorties
                    PiSortie, vSortie = self.resNeuro(othelliers)
                    pPi = self.pertePi(PisCible, PiSortie)
                    pV = self.perteV(vsCible, vSortie)
                    pertesTotales = pPi + pV

                    # Enregistrement des pertes moyennes
                    pertesPi.update(pPi.item(), othelliers.size(0))
                    pertesV.update(pV.item(), othelliers.size(0))

                    # Optimisation d'Adam
                    optimiseur.zero_grad()
                    pertesTotales.backward()
                    optimiseur.step()

                    # Mesure du temps écoulé
                    tpsCalcul.update(time.time() - fin)
                    fin = time.time()

                    # Affichage des informations
                    barre.set_postfix(donnees=tpsDonnees.moy, calcul=tpsCalcul.moy, pertesPi=pertesPi.moy, pertesV=pertesV.moy, refresh=False)

    def predire(self, othellier):
        """ Prédictions du réseau neuronal

        Arguments:
            othellier {np.array} -- Othellier actuel sous forme canonique

        Returns:
            (np.array, float) -- Vecteur des estimations Pi de l'othellier actuel, de
                                 dimension othello.getNombreActions()
                                 Ratio v des V/D de l'othellier actuel (compris dans [-1, 1])
        """
        othellier = torch.FloatTensor(othellier.astype(np.float64))
        if args.cuda:
            othellier = othellier.contiguous().cuda()
        othellier = othellier.view(1, self.dimX, self.dimY)
        self.resNeuro.eval()
        with torch.no_grad():
            Pi, v = self.resNeuro(othellier)
        return torch.exp(Pi).data.cpu().numpy()[0], v.data.cpu().numpy()[0]

    def pertePi(self, cibles, sorties):
        return -torch.sum(cibles * sorties) / cibles.size()[0]

    def perteV(self, cibles, sorties):
        return torch.sum((cibles - sorties.view(-1))**2) / cibles.size()[0]

    def sauverReprise(self, dossier='./modeles/reprises/', fichier='reprise.pth.tar'):
        """ Sauvegarde le réseau neuronal actuel et ses paramètres dans
        dossier\fichier

        Arguments:
            dossier {str} -- Emplacement du dossier
            fichier {str} -- Nom du fichier
        """
        cheminFichier = os.path.join(dossier, fichier)
        if not os.path.exists(dossier):
            print("Le dossier de sauvegarde n'existe pas, création du dossier {0}".format(dossier))
            os.mkdir(dossier)
        torch.save({
            'state_dict': self.resNeuro.state_dict(),
        }, cheminFichier)

    def chargerReprise(self, dossier='./modeles/reprises/', fichier='reprise.pth.tar'):
        """ Charge les paramètres du réseau neuronal depuis dossier\fichier

        Arguments:
            dossier {str} -- Emplacement du dossier
            fichier {str} -- Nom du fichier
        """
        cheminFichier = os.path.join(dossier, fichier)
        assert(os.path.exists(cheminFichier))
        map_location = None if args.cuda else 'cpu'
        reprise = torch.load(cheminFichier, map_location=map_location)
        self.resNeuro.load_state_dict(reprise['state_dict'])
