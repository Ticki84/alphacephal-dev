class Moyenne(object):
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.moy = 0
        self.somme = 0
        self.total = 0

    def update(self, val, n=1):
        self.val = val
        self.somme += val * n
        self.total += n
        self.moy = self.somme / self.total
