from time import sleep
import numpy as np


class Arene():
    """
    Une classe qui permet de faire combattre deux joueurs
    """

    def __init__(self, joueur1, joueur2, othello, afficher=None, interface=None):
        """ Initialisation de la classe

        Arguments:
            joueur1 {fct} -- Fonction jouer() du joueur 1
            joueur2 {fct} -- Fonction jouer() du joueur 2
            othello {Othello} -- Objet othello

        Keyword Arguments:
            afficher {fct} -- Fonction qui affiche l'Othello (default: {None})
            interface {Interface} -- Classe qui gère l'interface graphique (default: {None})
        """
        self.joueur1 = joueur1
        self.joueur2 = joueur2
        self.othello = othello
        self.afficher = afficher
        self.interface = interface
        self.nomJ1 = "Joueur 1"
        self.nomJ2 = "Joueur 2"
        if self.interface:
            self.nomJ1 = self.interface.nomJ1
            self.nomJ2 = self.interface.nomJ2
        self.inverse = False

    def jouerPartie(self, infos=False):
        """ Lance une partie d'Othello

        Keyword Arguments:
            infos {bool} -- True si on souhaite afficher davantage d'informations (default: {False})

        Returns:
            int -- 1: joueur1 a gagné, -1: joueur1 a perdu, autre valeur != 0: match nul
        """
        joueurs = [self.joueur2, None, self.joueur1]  # [-1, 0, 1]
        self.joueurActuel = 1                                 # ^ --> noir (X)
        self.othellier = self.othello.getOthellierInitial()
        tour = 0
        if self.interface:
            self.interface.listeCoups = ["Début de la partie"]
            self.interface.listeOthelliers = [(np.copy(self.othellier), self.joueurActuel, '~~', '~~')]
            self.interface.debutPartie(self.othellier, self.joueurActuel)
        while self.othello.getFinPartie(self.othellier, self.joueurActuel) == 0:
            tour += 1
            if self.interface:
                self.interface.mettreAJour(self.othellier, self.joueurActuel)
                if joueurs[self.joueurActuel + 1] != self.interface.jouer:
                    sleep(self.interface.tpsMinTour)
            if infos:
                assert self.afficher
                print("\nTour " + str(tour) + ": " + (self.nomJ1 + " (X)" if self.joueurActuel == 1 else self.nomJ2 + " (O)"))
                self.afficher(self.othello, self.othellier, self.joueurActuel)
            action = joueurs[self.joueurActuel + 1](self.othello.getFormeCanonique(self.othellier, self.joueurActuel))  # Appelle la fonction jouer() du joueur suivant avec le damier actuel en paramètre
            if action == self.othello.n**2 + 1:
                continue  # Si on veut revenir à un état précédent de l'Othellier

            coups = self.othello.getCoupsValides(self.othello.getFormeCanonique(self.othellier, self.joueurActuel), 1)  # On vérifie que le coup est valide
            if coups[action] == 0:
                print("Erreur:", action, "n'est pas une action valide")
                assert coups[action] > 0
            txtAction = (str(action // self.othello.n) + str(action % self.othello.n)) if action != self.othello.n**2 else '~~'
            if self.interface:
                multJ = -1 if self.inverse else 1
                self.interface.canevas.itemconfig(self.interface.coupG if self.joueurActuel * multJ == 1 else self.interface.coupD, text=txtAction)
                if action == self.othello.n**2:
                    self.interface.passTour = True
                    self.interface.canevas.itemconfig(self.interface.statut, text=(self.nomJ1 if self.joueurActuel == 1 else self.nomJ2) + " passe son tour", state='normal')
                    self.interface.fen.wait_variable(self.interface.case)
            self.othellier, self.joueurActuel = self.othello.getProchainEtat(self.othellier, self.joueurActuel, action)  # On joue le coup, on actualise l'othellier et on alterne le joueur
            if self.interface:
                self.interface.listeCoups.append((txtAction if action != self.othello.n**2 else 'Pass.') + " ~ " + (self.nomJ2 if self.joueurActuel == 1 else self.nomJ1))
                self.interface.listeOthelliers.append((np.copy(self.othellier), self.joueurActuel,
                                                       txtAction if self.joueurActuel * multJ == -1 else self.interface.listeOthelliers[-1][2],
                                                       txtAction if self.joueurActuel * multJ == 1 else self.interface.listeOthelliers[-1][3]))
        resultat = self.othello.getFinPartie(self.othellier, 1)
        if infos:
            assert self.afficher
            print("\nFin de la partie (tour " + str(tour) + ")")
            print(((self.nomJ1 if resultat else self.nomJ2) + " a gagné!") if resultat != -0.5 else "Match nul!")
            self.afficher(self.othello, self.othellier, self.joueurActuel)
        if self.interface:
            self.interface.finPartie(self.othellier, self.joueurActuel, resultat)
        return resultat

    def jouerParties(self, infos=False):
        """ Lancer des parties d'Othello indéfiniemment

        Keyword Arguments:
            infos {bool} -- True si on souhaite afficher davantage d'informations (default: {False})
        """
        while True:
            self.jouerPartie(infos)
            # On inverse les joueurs à chaque partie
            self.inverse = not self.inverse
            self.joueur1, self.joueur2 = self.joueur2, self.joueur1
            self.nomJ1, self.nomJ2 = self.nomJ2, self.nomJ1
            if self.interface:
                self.interface.inversionJoueurs()
