from classes.Entraineur import Entraineur
from classes.Othello import Othello
from classes.ReseauNeuro import ReseauNeuro
from classes.utils import dotdict
from multiprocessing import cpu_count
from math import ceil

"""
Ce fichier permet de choisir les paramètres de l'entraînement et de le débuter/reprendre
"""

if __name__ == '__main__':
    args = dotdict({
        'nbIterations': 3,                                            # Nombre d'itérations (= de modèles) à effectuer
        'nbParties': 5,                                               # Nombre de parties à jouer par itération
        'nbSimusMCTS': 5,                                             # Nombre de simulations de MCTS à effectuer par tour
        'seuilTour': 5,                                                # Seuil de tours des prévisions
        'seuilExemples': 100000,                                       # Seuil d'exemples
        'seuilRatioVictoire': 0.5,                                      # Ratio de V/D à atteindre contre le précedent modèle pour que le nouveau soit accepté
        'combatsComparaison': 5,                                       # Nombre de combats à effectuer pour comparer les modèles
        'cpuct': 1,                                                     # Hyperparamètre contrôlant le degrée d'exploration (à définir avant le premier entraînement, ne pas le changer après)
        'parallelisme': True,                                           # Active le parallélisme (pour faire du multi-processus)
        'nbCPU': cpu_count(),                                           # Nombre de coeurs à utiliser pour le parallélisme

        'reprise': './modeles/reprises/',                                       # Dossier où sauvegarder les reprises
        'chargerModele': False,                                                 # True pour reprendre depuis un précédent modèle
        'cheminModele': ('./modeles/reprises/', 'ancien.pth.tar'),              # Chemin vers le fichier du précédent modèle à charger
        'seuilHistoModeles': 15,                                                # Seuil de modèles (approuvés) dont les exemples sont conservés

    })

    if not args.parallelisme:
        args.nbCPU = 1
    # On fait du nombre de parties à jouer le multiple entier supérieur du nombre saisie
    args.nbParties = ceil(args.nbParties / args.nbCPU) * args.nbCPU
    args.combatsComparaison = ceil(args.combatsComparaison / args.nbCPU) * args.nbCPU
    args.combatsComparaison += args.combatsComparaison % 2

    othello = Othello(6)
    resNeuro = ReseauNeuro(othello)
    entraineur = Entraineur(othello, args)
    if args.chargerModele:
        print("Chargement des exemples précédents depuis le fichier 'historiqueExemples.pth.tar.exp'")
        entraineur.chargerExemples()
    entraineur.apprendre()
